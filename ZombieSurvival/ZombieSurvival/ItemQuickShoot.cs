﻿/*
 * /*
 Antonio defez
 ies san vicente
 Item type that increases player bullets
 */

public class ItemQuickShoot : Item
{
    public const string ITEM_IMAGE = "imgs/quickshoot.png";

    public ItemQuickShoot() : base(ITEM_IMAGE)
    {

    }

    public override void EffectItem(Character player)
    {

        base.EffectItem(player);
        player.TimeElapse -= 0.1;
    }
}
