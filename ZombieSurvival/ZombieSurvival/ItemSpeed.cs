﻿
/*
 Antonio defez
 ies san vicente
 Item type that increases player speed
 */



public class ItemSpeed : Item
{
    public const string ITEM_IMAGE = "imgs/fast.png";

    public ItemSpeed() : base(ITEM_IMAGE)
    {

    }

    public override void EffectItem(Character player)
    {

        base.EffectItem(player);
        player.Movement_increment += 1;
    }
}
