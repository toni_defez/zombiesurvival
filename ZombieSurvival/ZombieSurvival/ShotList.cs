﻿/*Antonio defez
 19/05/2017
 This class takes care of the set of shots of a sprite 
 (they can shoot character and enemy fixed)
 Its functionalities are draw, collision ..*/

using System.Collections.Generic;
using System;
public class ShotList :ListSprite
{
    protected List<Shot> shotList;
    protected int lastShootTime;
    protected double timeElapsed;

    public ShotList(double timeElapsed)
    {
        shotList = new List<Shot>();
        lastShootTime = 0;

        //
        this.timeElapsed = timeElapsed;
    }

    public override void Add(Sprite shot)
    {
        int currentShootTime = DateTime.Now.Second;
        if (lastShootTime >= 60)
            lastShootTime = 0;

        if (Math.Abs((double)lastShootTime-(double)currentShootTime)>timeElapsed)
        {
            shotList.Add((Shot)shot);
            lastShootTime = DateTime.Now.Second;
        }
    }

    public bool CanShoot()
    {
        int currentShootTime = DateTime.Now.Second;
        return Math.Abs((double)lastShootTime - (double)currentShootTime) > timeElapsed;
    }
    public override Sprite Get(int cont)
    {
        return shotList[cont];
    }

    public override void Set(Sprite otherSprite,int index)
    {
        shotList[index] = (Shot)otherSprite;
    }

    public override int Count()
    {
        return shotList.Count;
    }

   public void MoveShoot()
    {
        //move shoot   
        foreach (Shot s in shotList)
            s.Move();
    }

    //Draw all element of the shotlist and check status
    public  override void  Draw(Hardware hardware)
    {
        for (int i = 0; i < shotList.Count; i++)
        {
            if (shotList[i].CheckCollsion)
            {
                shotList.Remove(shotList[i]);
            }
            else
            {
                shotList[i].Draw(hardware);
            }
        }
    }
}

