﻿/*
 Antonio Defez Ibanez
 ies san vicente
 This class will store all the information and instructions for
 the main character
 */

public class Character : Sprite
{
    public const string CHARACTER_IMAGE = "imgs/character1.png";
    public const int CHARACTER_IMAGE_WIDTH = 32;
    public const int CHARACTER_IMAGE_HEIGHT = 32;
    private int lives;
    private int score;
    protected ShotList characterShotList;
    private int bulletCounter;



    public int Lives
    {
        get
        {
            return lives;
        }

        set
        {
            lives = value;
        }
    }

    public  int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
        }
    }

    public int BulletCounter
    {
        get
        {
            return bulletCounter;
        }

        set
        {
            bulletCounter = value;
        }
    }

    public Character() : base(CHARACTER_IMAGE, CHARACTER_IMAGE_WIDTH, 
        CHARACTER_IMAGE_HEIGHT)
    {
        lives = 4;
        Movement_increment = 5;
        score = 0;
        TimeElapse = 0.5;
        characterShotList = new ShotList(TimeElapse);
        BulletCounter = 10;
    }

    public override void Draw(Hardware hardware)
    {
        base.Draw(hardware);
        characterShotList.Draw(hardware);
    }

    /*We add a new shot to the list*/
    public void AddShoot()
    {
        
        Shot current_shot = new Shot(Direction, this.GetX(), this.GetY());
        characterShotList.Add(current_shot);
        BulletCounter--;
       
    }

    public bool  CharacterCanShoot()
    {
        return BulletCounter != 0 && characterShotList.CanShoot();
    }

    public ShotList GetShotList()
    {
        return characterShotList;
    }

    //Whenever the character takes damage it will lose a life
    public void TakingDamage()
    {
        lives--;
    }

    public bool IsDead()
    {
        return lives <= 0;
    }

    /*
     * I check the collisions of the bullets of the enemies with
     *  the sprite character, if it collides it subtracts a life
     */
    public void BulletCollision(Enemy enemy)
    {
        ShotList enemyShotList = enemy.getShotList();
        for (int c_shot = 0; c_shot < enemyShotList.Count(); c_shot++)
        {
            if (this.CollidesWith(enemyShotList.Get(c_shot),
                                            Character.CHARACTER_IMAGE_WIDTH,
                                            Character.CHARACTER_IMAGE_HEIGHT,
                                            Shot.SHOT_IMAGE_WIDTH,
                                            Shot.SHOT_IMAGE_HEIGHT))
            {
                this.TakingDamage();
                enemyShotList.Get(c_shot).AfterCollisionMap();
            }
        }
    }
}

