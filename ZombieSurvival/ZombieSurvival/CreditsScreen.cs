﻿/*
 Antonio Defez Ibanez
 Ies san vicente

 Changelog: 16/05/2017
 Created a struct with the name and the score of
 the best players. Then it is stored in an array
 of high scores, that display the best 5 players'
 name and highscore
 */

using System;
using System.IO;


public class CreditsScreen : Screen
{
    public struct HighScore
    {
        public int Score;
        public string Name;
    }

    const string CREDITS_IMAGE = "imgs/credits.png";
    //Path of the text file that stores the high scores
    const string HIGH_SCORE_FILE = "highscores.sav";
    const int CREDITS_IMAGE_WIDTH = 318;
    const int CREDITS_IMAGE_HEIGHT = 192;
    const int NUM_OF_HIGH_SCORES = 5;
    //Array of structs that stores the High Scores
    protected HighScore[] highScores;
    protected Image imgCredits;
    protected Font myFont;

    public CreditsScreen(Hardware hardware) : base(hardware)
    {
        highScores = new HighScore[NUM_OF_HIGH_SCORES];
        imgCredits = new Image(CREDITS_IMAGE, CREDITS_IMAGE_WIDTH, 
                                    CREDITS_IMAGE_HEIGHT);
        myFont = new Font(Hardware.GAME_FONT, 20);
        Load();
    }

    public override void Show()
    {
        Load();
        do
        {
            hardware.ClearScreen();
            //Writing the title of the screen
            hardware.WriteText("High scores: ", Hardware.DEFAULT_WIDTH / 2 - 200,
                                Hardware.DEFAULT_HEIGHT - 500,
                                255, 255, 0, myFont);
            for (int i = 0; i < NUM_OF_HIGH_SCORES; i++)
            {
                //Printing each of the high scores
                hardware.WriteText(highScores[i].Name,
                    Hardware.DEFAULT_WIDTH / 2 - 200,
                    (short)(Hardware.DEFAULT_HEIGHT / 2 - 40 * i), 255, 255, 0,
                    myFont);
                hardware.WriteText(highScores[i].Score.ToString("D5"),
                    Hardware.DEFAULT_WIDTH / 2 + 50,
                    (short)(Hardware.DEFAULT_HEIGHT / 2 - 40 * i), 255, 255, 0,
                    myFont);

               
            }
            hardware.UpdateScreen();

            System.Threading.Thread.Sleep(10);
        }
        while (hardware.KeyPressed() != Hardware.KEY_SPACE); 
    }

    /* CheckHighScore method to be called when the game ends, to check if the score is higher
    than the previously saved high scores. If it is higher, then it enters to the 
    high scores array, that is resorted from higher to lower. */
    public void CheckHighScore(int score, string name)
    {
        bool highScoreFound = false;
        for (int i = 0; i < NUM_OF_HIGH_SCORES && !highScoreFound; i++)
        {
            if (score > highScores[i].Score)
            {
                highScores[i].Name = name;
                highScores[i].Score = score;
                highScoreFound = true;
            }
        }
        Sort();
        Save();
    }

    /* Bubblesort to sort high scores (Array.Sort won't work right
    this time) */
    public void Sort()
    {
        for (int i = 0; i < NUM_OF_HIGH_SCORES - 1; i++)
        {
            for (int j = i + 1; j < NUM_OF_HIGH_SCORES; j++)
            {
                if (highScores[i].Score > highScores[j].Score)
                {
                    HighScore temp = highScores[i];
                    highScores[i] = highScores[j];
                    highScores[j] = temp;
                }
            }
        }
    }

    public void Save()
    {
        try
        {
            StreamWriter writer = File.CreateText(HIGH_SCORE_FILE);
            foreach (HighScore highScore in highScores)
            {
                writer.WriteLine(highScore.Name + ";" + highScore.Score);
               
            }
            writer.Close();
        }
        catch (PathTooLongException)
        {
            Console.WriteLine("Error: PathTooLongException");
        }
        catch (IOException e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: "+e.Message);
        }
    }

   

    public void Load()
    {
        try
        {
            StreamReader reader = File.OpenText(HIGH_SCORE_FILE);
            for (int i = 0; i < NUM_OF_HIGH_SCORES; i++)
            {
                string highScoreString = reader.ReadLine();
                string[] highScoreArray = highScoreString.Split(';');
                highScores[i].Name = highScoreArray[0];
                highScores[i].Score = Convert.ToInt32(highScoreArray[1]);
            }
            reader.Close();
            Sort();
        }
        catch (PathTooLongException)
        {
            Console.WriteLine("Error: PathTooLongException");
        }
        catch (IOException e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
    }
}
