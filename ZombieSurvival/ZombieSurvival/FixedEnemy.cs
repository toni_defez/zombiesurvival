﻿/*
 Antonio Defez Ibanez
 ies san vicente
 This method checks the contents of the EnemyFixe , this enemy can't move
 */
using System;
class FixedEnemy : Enemy
{
    public const  string ENEMY_IMAGE_FIXED = "imgs/FixedEnemy.png";
    public  const  int BLOCK_ID=28;
    protected ShotList enemyShotList;
    protected Random r;

    public FixedEnemy() : base(ENEMY_IMAGE_FIXED)
    {
        //The base class has everything that we need, 
        //so we don't have to add or modify any additional data
        TimeElapse = 0.5;
        enemyShotList = new ShotList(TimeElapse);
       
        r = new Random();
    }

    /*
     * The fixed enemy will have a random behavior and shoot in vertical or 
     * horizontal depending on random factors
     */
    public override void Move(Character CharacterToFollow, int level_width, int level_height)
    {
        int key = -3;
        int behaviour = r.Next(0, 2);
        if (this.DistanceToObject(CharacterToFollow, level_width, level_height))
        {
            if (behaviour == 0)
            {
                if (CharacterToFollow.GetX() > this.GetX())
                    key = Sprite.MOVE_RIGHT;

                if (CharacterToFollow.GetX() < this.GetX())
                    key = Sprite.MOVE_LEFT;
            }

            else
            {
                if (CharacterToFollow.GetY() > this.GetY())
                    key = Sprite.MOVE_DOWN;
                else
                    key = Sprite.MOVE_UP;
            }
            Direction = key;
            AddShoot();
        }
    }


    public void AddShoot()
    {
        
        Shot current_shot = new Shot(Direction, this.GetX(), this.GetY());
        enemyShotList.Add(current_shot);
    }

    public override void Draw(Hardware hardware)
    {
        base.Draw(hardware);
        enemyShotList.Draw(hardware);
    }

    public override ShotList getShotList()
    {
        return enemyShotList;
    }
}

