﻿/*
Antonio Defez Ibanez
ies san vicente
This class creates the basic unit for map formation that is the block.
*/

public class Block : Image
{
    private bool isEnemy;
    public const int COLLISION_ID= 11;
    public const int FINAL_ID = 6;
    public  const short WIDTH_BLOCK= 32;
    public const short HEIGHT_BLOCK = 32;
    private int index;                   
    private bool collision_check;         //check if we can collide with it
    public  bool Collision_check
    {
        get
        {
            return collision_check;
        }

        set
        {
            collision_check = value;
        }
    }
    public  int Index
    {
        get
        {
            return index;
        }

        set
        {
            index = value;
        }
    }
    public bool IsEnemy
    {
        get
        {
            return isEnemy;
        }

        set
        {
            isEnemy = value;
        }
    }

    public bool IsFinal
    {
        get
        {
            return isFinal;
        }

        set
        {
            isFinal = value;
        }
    }

    private bool isFinal;


    public Block(string fileName, bool collision,int index,bool final) :
        base(fileName, WIDTH_BLOCK,HEIGHT_BLOCK)
    {
        this.Index = index;
        this.IsFinal = final;
        this.collision_check = collision;
    }
}

