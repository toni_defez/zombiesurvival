﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */
//third implementatio of Map class

/*The type of blocks on the map decide how many enemies are on this map, as well
as their location.
Also decides the initial situation of the player as well as the final block 
of the level
*/
using System;
using System.Xml;

class Map
{
    protected string map_img_url;
    /*We leave an empty space between the screen and the drawing of the map so that the gui*/
    public const int GUI_HEIGHT = Gui.GUI_IMAGE_HEIGHT;
    
    protected int total_height_block;
    protected int total_width_block;
    protected  Block[,] array;
    protected byte[,] matrix;
    protected int total_enemy;

    protected int total_x;      //variables for scroll
    protected int total_y;      //variables for scroll
    protected int map_x;
    protected int map_y;
    public int TOTAL_X
    {
        get { return total_x; }
        set { total_x = value; }
    }
    public int TOTAL_Y
    {
        get { return total_y; }
        set { total_y = value; }
    }
    public int MAP_X
    {
        get { return map_x; }
        set { map_x = value; }
    }
    public int MAP_Y
    {
        get { return map_y; }
        set { map_y = value; }
    }

    private bool isFinished;
    public  bool IsFinished
    {
        get
        {
            return isFinished;
        }

        set
        {
            isFinished = value;
        }
    }
    protected int width_map;
    public int WIDTH_MAP
    {
        get { return width_map; }
        set { width_map = value; }
    }
    protected int heigth_map;
    public int HEIGTH_MAP
    {
        get { return heigth_map; }
        set { heigth_map = value; }
    }

 

    //TO DO IMPLEMENT A GROUP OF BLOCK COLLISIONABLES
    protected const int collisionable = 7;
    public Map(int index)
    {
        Console.WriteLine("Map of level"+index);
        map_img_url = "maps/"+index+".tmx";
        TmxToBinary(map_img_url);

        Image[,] array = new Image[total_width_block, total_height_block];
        BuildMap(); 
        WIDTH_MAP= total_width_block * (int)Block.WIDTH_BLOCK;
        HEIGTH_MAP = total_height_block * (int)Block.HEIGHT_BLOCK;
        total_x = 0;
        total_y = 0;
        IsFinished = false;
    }

    //Converts a tmx file to a binary array that
    public void TmxToBinary(string map)
    {
        
        matrix = null;
        XmlTextReader reader = new XmlTextReader(map);
        int width = 0;
        int height = 0;
        int cont_total = 0;
        int cont_row = 0;
        int count_file = 0;
        string source = "";

        while (reader.Read())
        {
            switch (reader.NodeType)
            {
                case XmlNodeType.Element:

                    string name = reader.Name;
                    if (name == "layer")
                    {
                        /*We get the width and height of the map*/
                        width = Convert.ToInt32(reader.GetAttribute("width"));
                        height = Convert.ToInt32(reader.GetAttribute("height"));
                    }

                    else if (name == "image")
                    {
                        /*We get the image where the sprite sheet is 
                         * used on the map*/
                        source = reader.GetAttribute("source");
                    }

                    break;
                case XmlNodeType.Text:
                    //We begin to read the distribution of the
                    //different sprites that form the map set
                    string encodedString = reader.Value;
                    //We get all the map information in one-dimensional format
                    byte[] data = Convert.FromBase64String(encodedString);

                    //We pass the information to a two-dimensional matrix
                    matrix = new byte[height, width];
                    while (cont_total < data.Length)
                    {
                        if (count_file == width)
                        {
                            if (cont_row + 1 < height)
                                cont_row++;
                            count_file = 0;
                        }
                        if (cont_total < data.Length)
                            matrix[cont_row, count_file] = data[cont_total];
                        cont_total += 4;
                        count_file++;
                    }
                    total_width_block = width;
                    total_height_block = height;
                    break;
            }
        }
        //delay for console
        for (int t = 0; t < height; t++)
        {
            for (int to = 0; to < width; to++)
                Console.Write(matrix[t, to]);
            Console.WriteLine();
        }
    }

    public Block[,] GetArray()
    {
        return array;
    }

    public void BuildMap()
    {
        // 1 -> For each iteration of matrix I have to create an image
        // with new images_file
        // 2 -> the images have a width and a height of 32
        // 3 -> place them in the same way
        // name of files -> 1_1.png
        array = new Block[total_width_block, total_height_block];
        for (int co_row = 0; co_row < total_height_block; co_row++)
            for (int f_file = 0; f_file < total_width_block; f_file++)
            {
                string filename = matrix[co_row, f_file] + "_1" + ".png";
                //block of enemy
                if (matrix[co_row, f_file] == MovingEnemy.BLOCK_ID
                    || matrix[co_row, f_file] == FixedEnemy.BLOCK_ID)
                {
                    array[f_file, co_row] = new Block(filename
                                                  , false,
                                                  matrix[co_row, f_file],false);
                    array[f_file, co_row].IsEnemy = true;
                    total_enemy++;
                }
                //the only block with collison is non collisionable
                else if (matrix[co_row, f_file] == Block.COLLISION_ID)
                    array[f_file, co_row] = new Block(filename
                                                      , false,
                                                      matrix[co_row, f_file],false);
                else if (matrix[co_row, f_file] == Block.FINAL_ID)
                {
                    array[f_file, co_row] = new Block(filename
                                  , false,
                                  matrix[co_row, f_file],true);
                }
                else
                {
                    //the rest of block will be collisonable
                    array[f_file, co_row] = new Block(filename
                                                      , true,
                                                      matrix[co_row, f_file],false);
                }
                array[f_file, co_row].SetX((short)(f_file * Block.HEIGHT_BLOCK));
                array[f_file, co_row].SetY((short)((co_row * Block.WIDTH_BLOCK)
                                                    + GUI_HEIGHT));
            }
    }

    public void Draw(Hardware hardware)
    {
        Block[,] array = this.GetArray();
        foreach (Block block in array)
        {
            if (block != null)
            {
                block.MoveTo((short)(block.GetX()), (short)(block.GetY()));
                hardware.DrawImage(block);
            }
        }
    }

    public void CheckCollision(Sprite sprite )
    {
        Image imgCharacter = sprite.getImage();
        foreach (Block t in array)
        {
            if (imgCharacter.CollidesWith(t, (short)sprite.GetSpriteWidth(),
                                             (short)sprite.GetSpriteHeight(),
                                             (short)Block.WIDTH_BLOCK,
                                             (short)Block.WIDTH_BLOCK))
            {
                if (t.Collision_check)
                {
                    sprite.AfterCollisionMap();
                }
                if (t.IsFinal)
                {
                    if (sprite.GetType().ToString() == "Character")
                    {
                        IsFinished = true;
                    }
                }
                //TODO IMPLEMENTS BLOCK DESTROYED


            }
        }
    }

    public void CheckCollisonGroup(ListSprite spriteGroup)
    {

        for (int i = 0; i < spriteGroup.Count(); i++)
        {
            CheckCollision(spriteGroup.Get(i));
        }
    }

    public int GetTotalEnemy()
    {
        return total_enemy;
    }


    //To implement the scroll we will have to move the entire map set block
    //by block.This movement will be defined by the movement of the player
    public void MoveMap()
    {
        foreach (Image t in array)
        {
            if (t != null)
            {
                t.MoveTo((short)(t.GetX() + map_x), (short)(t.GetY() +map_y));
            }
        }
        map_y = 0;
        map_x = 0;
    }
    /*
     * We have to increase the position of the enemies/bullets depending on the scroll
     */

    public void ScrollEnemyShoot(EnemyGroup enemies,Character player)
    {
        for (int i = 0; i < enemies.Count(); i++)
        {
            Enemy current =(Enemy)enemies.Get(i);
            if (current.getShotList() != null)
            {
                LocatedSpritesScroll(current.getShotList());
                CheckCollisonGroup(current.getShotList());
                player.BulletCollision(current);
            }
        }
    }


    public void LocatedSpritesScroll(ListSprite spriteslist)
    {
        Sprite currentSprite = null;
        for(int i=0; i<spriteslist.Count();i++)
        {
            currentSprite = spriteslist.Get(i);
            if (currentSprite != null)
            {
               currentSprite.MoveTo((short)(currentSprite.GetX() + map_x),
                    (short)(currentSprite.GetY() + map_y));
            }
        }
           
       
    }

}
