﻿/*
 Antonio Defez
 ies san vicente
 
This class is in charge of controlling the set of items and 
also deciding which item to create next
 */


using System.Collections.Generic;
using System; 
public class ItemList:ListSprite
{
    protected List<Item> items;
    private Random r_item;
    public ItemList()
    {
        items = new List<Item>();
        r_item = new Random();
    }

    public override void Add(Sprite otherSprite)
    {
        items.Add((Item)otherSprite);
    }

    public override int   Count()
    {
        return items.Count;
    }

    //TODO THIS CODE IS EXACLY THE SAME OF LISTSHOOT
    //IT MUST BE HERATY OF SHOOT ?
    //
    public override Sprite Get(int cont)
    {
        return (Item)items[cont];
    }

    public void  AddNewItem(Sprite denemy)
    {
        /*
         * Usamos random y creamos un objeto al azar
         * por ahora podemos meter varios objetos
         * uno para la vida, otro para la velocidad*/
        int number_item = r_item.Next(0, 6);
        Item nexItem = null;
        switch (number_item)
        {
            case 0://Item healt
                nexItem=new ItemHealth();
                break;
            case 1://Item Velocity
                nexItem = new ItemSpeed();
                break;
            case 2://Item money
                nexItem = new ItemMoney();
                break;
            case 3: //item QuickScope
                nexItem = new ItemQuickShoot();
                break;
            case 4:
            case 5:
                nexItem = new ItemBullet();
                break;
        }
        nexItem.getImage().MoveTo(
                                    denemy.GetX(),
                                    denemy.GetY()
                                     );
                                     
        this.Add(nexItem);
    }

    public override void Draw(Hardware hardware)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].CheckCollsion)
            {
                items.Remove(items[i]);
            }
            else
            {
                items[i].Draw(hardware);
            }
        }
    }

}

