﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */
/*
* We will use this class as interface to speed up the creation of windows,
* and avoid using SDL-TAO directly
* changes:
* 0.1 Initial version, based on Proyect Centipede
 */

public class Screen
{
    protected Hardware hardware;

    public Screen(Hardware hardware)
    {
        this.hardware = hardware;
    }

    public virtual void Show()
    {
    }
}


