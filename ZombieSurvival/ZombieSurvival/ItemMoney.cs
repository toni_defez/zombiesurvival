﻿/*
 Antonio defez
 ies san vicente
 Item type that increases player bullet score
 */

public class ItemMoney : Item
{
    public const string ITEM_IMAGE = "imgs/money.png";

    public ItemMoney() : base(ITEM_IMAGE)
    {

    }

    public override void EffectItem(Character player)
    {

        base.EffectItem(player);
        player.Score += 100;
    }
}
