﻿/*
Antonio Defez Ibanez
ies san vicente
This class stores all the information of the Enemy class
 */
using System;

public class Enemy : Sprite
{
    protected bool isDead;
    public const int ENEMY_IMAGE_WIDTH = 32;
    public const int ENEMY_IMAGE_HEIGHT = 32;
    protected Audio audio;
    protected int numLives;
    public  bool IsDead
    {
        get
        {
            return isDead;
        }

        set
        {
            isDead = value;
        }
    }

    /*Empty builder for used in enemy group*/
    public Enemy():base()
    {
    }

    public Enemy(string img) : base(img, ENEMY_IMAGE_WIDTH, 
        ENEMY_IMAGE_HEIGHT)
    {
        isDead = false;
        audio = new Audio(44100, 2, 4096);
        numLives = 1;
    }

    //prevents Enemy from running across the map to get you
    public bool DistanceToObject(Sprite Object,int width,int height)
    {
        int totalx = this.GetX() - Object.GetX();
        int totaly = this.GetY() - Object.GetY();

        double total = Math.Sqrt(Math.Pow(totalx, 2) + Math.Pow(totaly, 2));

        return (total < width) || (total < height);
    }

    /*
     * FixedEnemy and MovingEnemy move differently
     */
    public virtual void Move(Character imgCharacter,int level_width,int level_height)
    {
        
    }

    public override void Draw(Hardware hardware)
    {
        base.Draw(hardware);
    }

    public override void AfterCollisionBullet()
    {
        numLives--;
        if(numLives<=0)
            
            isDead = true;
    }


    public virtual ShotList getShotList()
    {
        return null;
    }

}

