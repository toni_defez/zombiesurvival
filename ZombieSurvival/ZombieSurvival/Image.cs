﻿/*(c)Antonio Defez
 * We will use this class as interface to speed up the creation of imagens,
 * and avoid using SDL-TAO directly
 * To this class we will send the parameters of width, height, direction_image
 */ 
using System;
using Tao.Sdl;


public class Image
{
 
    protected short x, y;
    protected short imageWidth, imageHeight;
    protected IntPtr image;

    public Image(string fileName, short width, short height)
    {
        image = SdlImage.IMG_Load(fileName);
  
        if (image == IntPtr.Zero)
        {

            Console.WriteLine("Image not found");
            Environment.Exit(1);
        }
        imageWidth = width;
        imageHeight = height;
    }

    public void MoveTo(short x, short y)
    {
        this.x = x;
        this.y = y;
    }

    public short GetX()
    {
        return x;
    }

    public short GetY()
    {
        return y;
    }

    public void SetX(short x)
    {
        this.x = x;
    }

    public void SetY(short y)
    {
        this.y = y;
    }

    public short GetImageWidth()
    {
        return imageWidth;
    }

    public short GetImageHeight()
    {
        return imageHeight;
    }

    public IntPtr GetImage()
    {
        return image;
    }

    public virtual bool CollidesWith(Image img, short w1, short h1, short w2, short h2)
    {
        return (x + w1 >= img.x && x <= img.x + w2 &&
                y + h1 >= img.y && y <= img.y + h2);
    }

}