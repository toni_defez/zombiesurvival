﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */

class WelcomeScreen : Screen
{
    const string WELCOME_IMAGE = "imgs/zombie_index.png";
    const int WELCOME_IMAGE_WIDTH = 1000;
    const int WELCOME_IMAGE_HEIGHT = 250;
    protected Audio backgroundSong;
    protected Image imgTitle;
    bool exit;

    public WelcomeScreen(Hardware hardware) : base(hardware)
    {
        exit = false;
        backgroundSong = new Audio(44100, 2, 4096);


         imgTitle = new Image(WELCOME_IMAGE, WELCOME_IMAGE_WIDTH,
            WELCOME_IMAGE_HEIGHT);

       
        backgroundSong.AddMusic("music/intro.mid");
    }

    public override void Show()
    {
        bool escPressed = false, spacePressed = false, qPressed = false;
        CreditsScreen creditsScreen = new CreditsScreen(hardware);
        Font myFont = new Font(Hardware.GAME_FONT, 20);
        imgTitle.MoveTo(Hardware.DEFAULT_WIDTH / 2 - WELCOME_IMAGE_WIDTH / 2,
            Hardware.DEFAULT_HEIGHT / 3 - WELCOME_IMAGE_HEIGHT / 2);
        hardware.DrawImage(imgTitle);
        hardware.WriteText("Press SpaceBar to go on or Escape to exit",
            Hardware.DEFAULT_WIDTH / 2 - 250, Hardware.DEFAULT_HEIGHT * 2 / 3
            , 255, 255, 0, myFont);
        hardware.WriteText("Press 1 to see the high scores", 
            Hardware.DEFAULT_WIDTH / 2 - 175, Hardware.DEFAULT_HEIGHT / 3 + 225,
            255, 255, 0, myFont);
        hardware.UpdateScreen();

      
        backgroundSong.PlayMusic(0, -1);
        do
        {
          
            int keyPressed = hardware.KeyPressed();
            if (keyPressed == Hardware.KEY_ESC)
            {
                escPressed = true;
                exit = true;
            }
            else if (keyPressed == Hardware.KEY_SPACE)
            {
                spacePressed = true;
                exit = false;
            }
            else if (keyPressed == Hardware.KEY_1)
            {
                qPressed = true;
                creditsScreen.Show();
                exit = false;
            }
            //Pause
            System.Threading.Thread.Sleep(10);
        }
        while (!escPressed && !spacePressed && !qPressed);
       
    }

    public bool GetExit()
    {
        return exit;
    }
}

