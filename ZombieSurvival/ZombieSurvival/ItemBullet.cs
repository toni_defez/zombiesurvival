﻿/*
 Antonio defez
 ies san vicente
 Item type that increases player bullets
 */


public class ItemBullet : Item
{
    public const string ITEM_IMAGE = "imgs/health.png";

    public ItemBullet() : base(ITEM_IMAGE)
    {

    }

    public override void EffectItem(Character player)
    {

        base.EffectItem(player);
        player.BulletCounter += 10;
    }
}
