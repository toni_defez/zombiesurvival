﻿/*(c)Antonio defez
* 
* It is a list of sprite that gathers the basic information of lists
*/
public class ListSprite
{

    public virtual Sprite Get(int cont)
    {
        return null;
    }

    public  virtual int Count()
    {
        return 0;
    }

    public virtual void  Add(Sprite otherSprite)
    {
    }

    public virtual void Set(Sprite otherSprite,int index)
    {

    }

    public virtual void Draw(Hardware hardware)
    {
    }
}