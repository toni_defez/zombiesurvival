﻿/*
 Antonio Defez Ibanez
 ies san vicente
 
Each level is a screen, in this screen we will find all elemntos of the game.
The map, enemies, player.
In this class there is also the game loop.
And the gui structure that defines the player's gui
Change 16/05/2017: Added call to CheckScore method from
CreditsScreen
 */


class Level:Screen
{
    //const for level
    const int LEVEL_IMAGE_WIDTH = 318;
    const int LEVEL_IMAGE_HEIGHT = 192;

    protected bool finished;
    protected int index;
    protected Map mapLevel;


    protected Character player;
    protected EnemyGroup enemiesGroup;
    protected Gui guiPlayer;
    protected int increment_scroll;

    protected Audio audio;

    public Level(Hardware hardware, int index,
        ref Character  MainCharacter) : base(hardware)
    {
        this.index = index;
        this.mapLevel = new Map(index);
        finished = false;
        player = MainCharacter;
        guiPlayer = new Gui(player);
        enemiesGroup = new EnemyGroup(mapLevel.GetTotalEnemy());
        //Put all enemies on the map
        enemiesGroup.AddGroupToMap(mapLevel);
        increment_scroll = 5;
        audio = new Audio(44100, 2, 4096);
    }

    public override void Show()
    {
        
        Font myFont = new Font(Hardware.GAME_FONT, 20);
        player.getImage().MoveTo(200, 200);
        audio.AddWAV("music/mario_bump.wav");
        audio.AddMusic("music/"+index+".mid");

        audio.PlayMusic(1, -1);

        while (!finished && !player.IsDead() )
        {
            // 1. Draw everything
            Draw();
            //2.Chek keys
            CheckKeys();
            // 3. Move other elements (automatically)
            AutomaticMovement();
            
            // 4. Collision detection
            CheckCollision();
            // 5. Pause game
            System.Threading.Thread.Sleep(10);
        }
        finished = true;
    }

    public void AutomaticMovement()
    {
        enemiesGroup.Move(player, Level.LEVEL_IMAGE_WIDTH,
                Level.LEVEL_IMAGE_HEIGHT);
        //Move shots
        player.GetShotList().MoveShoot();
        //Move shotsenemy
        enemiesGroup.EnemyGroupShoots();

        guiPlayer.Update(player);
        //scroll enemies /shot /items
        mapLevel.LocatedSpritesScroll(player.GetShotList());
        mapLevel.LocatedSpritesScroll(enemiesGroup.ItemList);
        //move enemies
        mapLevel.LocatedSpritesScroll(enemiesGroup);
        //move shotlist of all enemies
        mapLevel.ScrollEnemyShoot(enemiesGroup,player);
        mapLevel.MoveMap();

    }

    public void Draw()
    {
        hardware.ClearScreen();
        mapLevel.Draw(hardware);
        enemiesGroup.Draw(hardware);
        player.Draw(hardware);
       
        guiPlayer.Draw(hardware);
        hardware.UpdateScreen();
    }

    /*Depending on where the character moves the map will scroll or will not do 
     * it.Compiling as long as you do not leave the map
     */
    public void Move_left()
    {
        if (mapLevel.TOTAL_X <= 0 && player.GetX() <= Hardware.DEFAULT_WIDTH / 2)
        {
            player.Direction=Character.MOVE_LEFT;
            player.UpdateAnim(Character.MOVE_LEFT);
            mapLevel.MAP_X += increment_scroll;
            mapLevel.TOTAL_X += increment_scroll;
        }
        else if (player.GetX() > 0)
        {
            player.Move_Sprite(Sprite.MOVE_LEFT);
        }
    }

    public void Move_right()
    {
        if (mapLevel.TOTAL_X >= -mapLevel.WIDTH_MAP + Hardware.DEFAULT_WIDTH
                 &&
                 player.GetX() >= Hardware.DEFAULT_WIDTH / 2)
        {
            player.Direction=Character.MOVE_RIGHT;
            player.UpdateAnim(Character.MOVE_RIGHT);

            mapLevel.MAP_X -= increment_scroll;
            mapLevel.TOTAL_X -= increment_scroll;
        }
        else if (player.GetX() < Hardware.DEFAULT_WIDTH - player.GetSpriteWidth())
        {
            player.Move_Sprite(Sprite.MOVE_RIGHT);
        }
    }

    public void Move_up()
    {     
        if (mapLevel.TOTAL_Y <=0 &&
               player.GetY() <= Hardware.DEFAULT_HEIGHT / 2)
        {
            player.Direction=Character.MOVE_UP;
            player.UpdateAnim(Character.MOVE_UP);
            CheckCollision();
            mapLevel.MAP_Y += increment_scroll;
            mapLevel.TOTAL_Y += increment_scroll;
        }
        else if (player.GetY() <= Hardware.DEFAULT_HEIGHT - player.GetSpriteHeight()
            && player.GetY() > 0)
        {
            player.Move_Sprite(Sprite.MOVE_UP);
        }
    }

    public void Move_down()
    {
        if ( mapLevel.TOTAL_Y >= -mapLevel.HEIGTH_MAP + Hardware.DEFAULT_HEIGHT
            && player.GetY() > Hardware.DEFAULT_HEIGHT / 2)
        {
            player.Direction=Character.MOVE_DOWN;
            player.UpdateAnim(Character.MOVE_DOWN);
            mapLevel.MAP_Y -= increment_scroll;
            mapLevel.TOTAL_Y -= increment_scroll;
        }
        else if (player.GetY() < Hardware.DEFAULT_HEIGHT - player.GetSpriteHeight())
        {
            player.Move_Sprite(Sprite.MOVE_DOWN);
        }
    }

    /*Check Keyboard Events*/
    public void CheckKeys()
    {
        increment_scroll = player.Movement_increment;
        if (hardware.IsKeyPressed(Hardware.KEY_LEFT))
            Move_left();

        else if (hardware.IsKeyPressed(Hardware.KEY_RIGHT))
        {
            Move_right();
        }
        else if (hardware.IsKeyPressed(Hardware.KEY_DOWN))
        {
            Move_down();
        }
        else if (hardware.IsKeyPressed(Hardware.KEY_UP))
        {
            Move_up();
        }

        if (hardware.IsKeyPressed(Hardware.KEY_SPACE))
        {
            if (player.CharacterCanShoot())
            {
                audio.PlayWAV(0, 2, 0);

                player.AddShoot();
            }

        }
    }

    public void CheckCollision()
    {
        //checks collision between enemy and playe
        enemiesGroup.EnemyGroupCollides(player);
        //check collision between player and map
        mapLevel.CheckCollision(player);
        //chek collision between enmies and map
        mapLevel.CheckCollisonGroup(enemiesGroup);
        //check collsion between player's shot and map
        mapLevel.CheckCollisonGroup(player.GetShotList());

        //check collision between enemeies and
        mapLevel.CheckCollisonGroup(enemiesGroup.ItemList);
        enemiesGroup.BulletCollision(player);

        //Check if we have collided with the final block,
        //if we collided with that block we have passed the level
        finished = mapLevel.IsFinished;
    }

    public void setFinished(bool state)
    {
        this.finished = state;
    }

    public bool getFinished()
    {
        return finished;
    }

    public Character getCharacter()
    {
        return player;
    }
}

