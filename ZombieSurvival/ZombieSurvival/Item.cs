﻿/*
Antonio defez 
ies san vicente 
This class has all the information relevant to the class item. The operation 
of the same is as follows, when an enemy dies appears in its position a new 
item (there are 4 types of item) created randomly
 */


using System;
public class Item : Sprite
{
    public const int ITEM_IMAGE_WIDTH = 32;
    public const int ITEM_IMAGE_HEIGHT = 32;
    private bool checkCollsion;

    public bool CheckCollsion
    {
        get
        {
            return checkCollsion;
        }

        set
        {
            checkCollsion = value;
        }
    }

    public Item(string item_image) : base(item_image, ITEM_IMAGE_WIDTH,
        ITEM_IMAGE_HEIGHT)
    {
        checkCollsion = false;
    }


    public virtual   void EffectItem(Character player)
    {
        CheckCollsion = true;
        
    }
}

