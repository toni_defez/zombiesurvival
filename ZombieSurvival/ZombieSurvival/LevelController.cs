﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */
/*
This class is responsible for iterating between the different levels and 
checking its status

The main character is the one that is sent to all levels and serves to keep
information regarding your score, total lives 
*/

using System;

class LevelController 
{
    protected bool exit;
    protected  Level[] levels;
    protected Character MainCharacter;
    protected int currentLevel;
    protected Hardware hardware;
    protected int score_total;

    public LevelController(Hardware hardware) 
    {

        levels = new Level[3];
        MainCharacter = new Character();
        this.hardware = hardware;
        exit = false;
        
    }

    public  void Show_Level()
    {
        currentLevel = 0;

        for (int i = 0; i < levels.Length && !MainCharacter.IsDead(); i++)
        {
            Console.WriteLine(i);
            levels[i] = new Level(hardware, i,ref MainCharacter);
            while (!levels[i].getFinished())
                levels[i].Show();

            currentLevel = i;
        }
        score_total = MainCharacter.Score;
        if(MainCharacter.IsDead())
            //temporal Test
            Console.WriteLine("Player dead");
    }

    public void ResetGame()
    {
        //reboot all levels
        for (int i = 0; i <= currentLevel; i++)
            levels[i].setFinished(false);
        //reboot character
        MainCharacter = new Character();
    }

    public int GetScore()
    {
        return score_total;
    }

    public void SetScore(int score)
    {
        this.score_total = score;
    }

    public Character GetCharacter()
    {
        return MainCharacter;
    }

    public bool GetExit()
    {
        return exit;
    }
}
