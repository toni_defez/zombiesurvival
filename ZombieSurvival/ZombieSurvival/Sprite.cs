﻿/*
 *  Antonio defez
 *  The sprite class exists to cover all the functionalities that have 
 *  in common the elements that move.Animations ...
 */

public class Sprite
{
    public const int MOVE_UP= 3;
    public const int MOVE_LEFT = 1;
    public const int MOVE_RIGHT = 2;
    public const int MOVE_DOWN = 0;

    protected  short sprite_image_width ;
    protected short sprite_image_height ;
    protected Image image_sprite;
    protected string image_direction;
    private int movement_increment;
    protected int current_sprite_x;
    protected int current_sprite_y;
    private int direction;

    public int Direction
    {
        get
        {
            return direction;
        }

        set
        {
            direction = value;
        }
    }

    public int Movement_increment
    {
        get
        {
            return Movement_increment1;
        }

        set
        {
            Movement_increment1 = value;
        }
    }

    public int Movement_increment1
    {
        get
        {
            return movement_increment;
        }

        set
        {
            movement_increment = value;
        }
    }
    private double timeElapse;       //Break time between shots
    public  double TimeElapse
    {
        get
        {
            return timeElapse;
        }

        set
        {
            timeElapse = value;
        }
    }   
    public Sprite()
    {
        //Builder empty, I need this to easily create generic enemies
    }

    public Sprite(string image_direction,int width,int height )
    {
        sprite_image_width = (short)width;
        sprite_image_height = (short)height;
        image_sprite = new Image(image_direction, sprite_image_width,
                                                sprite_image_height);
    }

    public Sprite(string image_direction, int width, int height, int x, int y,
        int movement_increment)
        :this(image_direction, width, height)
    {
        this.image_sprite.SetX((short)x);
        this.image_sprite.SetY((short)y);
        this.Movement_increment = movement_increment;
    }

 


    public Image getImage()
    {
        return image_sprite; 
    }

    public virtual void Draw(Hardware hardware)
    {
        hardware.DrawImage(image_sprite, 
            (short)(current_sprite_x * sprite_image_width),
            (short)(current_sprite_y * sprite_image_height), sprite_image_width,
            sprite_image_height);
    }

    /*
    It receives that movement it will make next the sprite, moves the image
    according to the previous one and it connects it to its corresponding 
    animation*/
    public void  Move_Sprite(int cdirection)
    {
        Direction = cdirection;
        if (cdirection == Sprite.MOVE_LEFT)
        {
            image_sprite.MoveTo((short)(image_sprite.GetX() - Movement_increment),
            image_sprite.GetY());
            UpdateAnim(MOVE_LEFT);
        }
        else if (cdirection == Sprite.MOVE_RIGHT)
        {
            image_sprite.MoveTo((short)(image_sprite.GetX() + Movement_increment),
            image_sprite.GetY());
            UpdateAnim(MOVE_RIGHT);
        }
        else if (cdirection == Sprite.MOVE_UP)
        {
            image_sprite.MoveTo((short)(image_sprite.GetX()),
                                (short)(image_sprite.GetY() - Movement_increment));
            UpdateAnim(MOVE_UP);

        }
        else if (cdirection == Sprite.MOVE_DOWN)
        {
            image_sprite.MoveTo((short)(image_sprite.GetX()),
                                (short)(image_sprite.GetY() + Movement_increment));
            UpdateAnim(MOVE_DOWN);
        }
    }
    /*
     * Update variables to perform animation
     */
    public void UpdateAnim(int move)
    {
        current_sprite_y = move;
        if (current_sprite_x >= 2)
        {
            current_sprite_x = 0;
        }
        else
        {
            current_sprite_x++;
        }
    }

    public short GetX()
    {
        return image_sprite.GetX();
    }

    public short GetY()
    {
        return image_sprite.GetY();
    }


    public bool CollidesWith(Sprite OtherSprite, short w1, short h1, short w2, short h2)
    {
        Image img = OtherSprite.getImage();
        return image_sprite.CollidesWith(img, w1, h1, w2, h2);
    }

    public void MoveTo(short x, short y)
    {
        this.image_sprite.MoveTo(x, y);
    }

    public int GetSpriteWidth()
    {
        return image_sprite.GetImageWidth();
    }

    public int GetSpriteHeight()
    {
        return image_sprite.GetImageHeight();
    }

   


    /*
     * 
    After an impact, there will be a change in the state of the sprite
     */
    public virtual void  AfterCollisionMap()
    {
        if (this.direction == Sprite.MOVE_LEFT)
        {
            this.MoveTo((short)(this.GetX() + movement_increment),
                this.GetY());
        }
        else if (this.direction == Sprite.MOVE_RIGHT)
        {
            this.MoveTo((short)(this.GetX() - movement_increment),
                this.GetY());
        }
        else if (this.direction == Sprite.MOVE_UP)
        {
            this.MoveTo(this.GetX(),
                (short)(this.GetY() + movement_increment));
        }
        else if (this.direction == Sprite.MOVE_DOWN)
        {
            this.MoveTo(this.GetX(),
                (short)(this.GetY() - movement_increment));
        }
    }

    public virtual void AfterCollisionBullet()
    {

    }

}

