﻿/*
 Antonio Defez Ibanez
 ies san vicente

This enemy follows the character and it hurts him
 */


class MovingEnemy : Enemy
{
    public const string ENEMY_IMAGE_MOVEMENT = "imgs/movingEnemy.png";
    public const int BLOCK_ID = 27;

    public MovingEnemy() : base(ENEMY_IMAGE_MOVEMENT)
    {
        Movement_increment = 1;
    }

    /*
     * The enemy type movement will follow the player
     */
    public override void Move(Character CharacterToFollow, int level_width,
        int level_height)
    {
        Movement_increment = 1;
        int key = -3;
        if (this.DistanceToObject(CharacterToFollow, level_width, level_height))
        {
            if (CharacterToFollow.GetX() > this.GetX())
                key = Sprite.MOVE_RIGHT;

            else if (CharacterToFollow.GetX() < this.GetX())
                key = Sprite.MOVE_LEFT;

            else if (CharacterToFollow.GetY() > this.GetY())
                key = Sprite.MOVE_DOWN;
            else
                key = Sprite.MOVE_UP;

            base.Move_Sprite(key);
        }
    }
}

