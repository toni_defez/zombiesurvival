﻿/*
 Antonio Defez
 
Shows the player interface in the game, in this interface 
will appear data such as current life, playing time and player's score
 */


public class Gui
{
    protected string direction_guy;
    protected string direction_heart;
    protected Image gui_principal;
    protected int gui_width;
    protected int gui_height;

    protected Image heart;
    protected int heart_width;
    protected int heart_height;

    public const int GUI_IMAGE_WIDTH = 799;
    public const int GUI_IMAGE_HEIGHT = 70;
    public const int HEART_IMAGE_WIDTH = 500;
    public const int HEART_IMAGE_HEIGHT = 500;

    private int score;
    public  int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
        }
    }

    public int Numhearts
    {
        get
        {
            return numhearts;
        }

        set
        {
            numhearts = value;
        }
    }

    public int Numshoot
    {
        get
        {
            return numshoot;
        }

        set
        {
            numshoot = value;
        }
    }

    private int numhearts;
    private int numshoot;
 
    public Gui(Character player)
    {
        this.direction_guy = "imgs/gui.png";
        this.direction_heart = "imgs/live.png";
        this.gui_height = GUI_IMAGE_WIDTH;
        this.gui_width = GUI_IMAGE_HEIGHT;
        this.gui_principal = new Image(this.direction_guy, GUI_IMAGE_WIDTH,
            GUI_IMAGE_HEIGHT);
        this.heart = new Image(direction_heart, HEART_IMAGE_WIDTH,
           HEART_IMAGE_HEIGHT);
        this.score = player.Score;
        this.numhearts = player.Lives;
        this.numshoot = player.BulletCounter;
        
    }

    public void Update(Character player)
    {
        this.score = player.Score;
        this.numhearts = player.Lives;
        this.numshoot = player.BulletCounter;
    }

    public void  Draw(Hardware hardware)
    {
        hardware.DrawImage(this.gui_principal);
        Font myFont = new Font(Hardware.GAME_FONT, 20);
        hardware.WriteText("HP", 10, 20,
            255, 255, 0, myFont);
        hardware.WriteText("Score", 10, 40,
            255, 255, 0, myFont);
        hardware.WriteText(this.score.ToString("0000"), 100, 40,
            255, 255, 0, myFont);
        hardware.WriteText(this.numshoot.ToString("0000"), 600, 20,
             255, 255, 0, myFont);

        for (int i = 0; i < numhearts; i++)
        {
            this.heart.MoveTo((short)(200 + i * 50), 20);
            hardware.DrawImage(this.heart);
        }
    }
}

