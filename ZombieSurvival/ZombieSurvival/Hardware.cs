﻿/*(c)Antonio Defez*/
/*
 * We will use this class as interface to speed up the creation of windows,
 * and avoid using SDL-TAO directly
 * To this class we will send the parameters of width, height, color palette 
 * and if this screen completes to create the screen
 * 
 * changes:
 * 0.1 Initial version, based on Proyect Centipede 
 * 0.11 , 14-may-2017: Fixed Bug in WriteHiddenText
 * 
 */
using System;
using Tao.Sdl;

public class Hardware
{
    public const int DEFAULT_WIDTH = 800;
    public const int DEFAULT_HEIGHT = 600;

    public const string GAME_FONT = "resources/DAYPBL__.ttf";

    public static int KEY_ESC = Sdl.SDLK_ESCAPE;
    public static int KEY_UP = Sdl.SDLK_UP;
    public static int KEY_DOWN = Sdl.SDLK_DOWN;
    public static int KEY_LEFT = Sdl.SDLK_LEFT;
    public static int KEY_RIGHT = Sdl.SDLK_RIGHT;
    public static int KEY_SPACE = Sdl.SDLK_SPACE;

    short screenWidth;
    short screenHeight;
    short colorDepth;
    IntPtr screen;

    public Hardware(short width, short height, short depth, bool fullScreen)
    {
        screenWidth = width;
        screenHeight = height;
        colorDepth = depth;

        int flags = Sdl.SDL_HWSURFACE | Sdl.SDL_DOUBLEBUF | Sdl.SDL_ANYFORMAT;
        if (fullScreen)
            flags = flags | Sdl.SDL_FULLSCREEN;

        Sdl.SDL_Init(Sdl.SDL_INIT_EVERYTHING);
        screen = Sdl.SDL_SetVideoMode(screenWidth, screenHeight, colorDepth, flags);
        Sdl.SDL_Rect rect = new Sdl.SDL_Rect(0, 0, screenWidth, screenHeight);
        Sdl.SDL_SetClipRect(screen, ref rect);

        SdlTtf.TTF_Init();
    }

    ~Hardware()
    {
        Sdl.SDL_Quit();
    }

    public void DrawImage(Image img)
    {
        Sdl.SDL_Rect source = new Sdl.SDL_Rect(0, 0, img.GetImageWidth(),
            img.GetImageHeight());
        Sdl.SDL_Rect target = new Sdl.SDL_Rect(img.GetX(), img.GetY(), 
            img.GetImageWidth(), img.GetImageHeight());
        Sdl.SDL_BlitSurface(img.GetImage(), ref source, screen, ref target);
    }
    public void DrawImage(Image img, int x, int y)
    {
        Sdl.SDL_Rect source = new Sdl.SDL_Rect((short)x, (short)y,
            img.GetImageWidth(), img.GetImageHeight());
        Sdl.SDL_Rect target = new Sdl.SDL_Rect(img.GetX(), img.GetY(),
            img.GetImageWidth(), img.GetImageHeight());
        Sdl.SDL_BlitSurface(img.GetImage(), ref source, screen, ref target);
    }

    public void DrawImage(Image image, short x, short y, short width, short height)
    {
        Sdl.SDL_Rect src = new Sdl.SDL_Rect(x, y, width, height);
        Sdl.SDL_Rect dest = new Sdl.SDL_Rect(image.GetX(), image.GetY(),
            width, height);
        Sdl.SDL_BlitSurface(image.GetImage(), ref src, screen, ref dest);
    }

    public void WriteText(string text, short x, short y,
                                    byte r, byte g, byte b, Font fontType)
    {
        Sdl.SDL_Color color = new Sdl.SDL_Color(r, g, b);
        IntPtr textAsImage = SdlTtf.TTF_RenderText_Solid(fontType.GetFontType(),
            text, color);
        if (textAsImage == IntPtr.Zero)
            Environment.Exit(5);
        Sdl.SDL_Rect src = new Sdl.SDL_Rect(0, 0, screenWidth, screenHeight);
        Sdl.SDL_Rect dest = new Sdl.SDL_Rect(x, y, screenWidth, screenHeight);
        Sdl.SDL_BlitSurface(textAsImage, ref src, screen, ref dest);
        Sdl.SDL_FreeSurface(textAsImage);
    }

    public int KeyPressed()
    {
        int pressed = -1;

        Sdl.SDL_PumpEvents();
        Sdl.SDL_Event keyEvent;
        if (Sdl.SDL_PollEvent(out keyEvent) == 1)
        {
            if (keyEvent.type == Sdl.SDL_KEYDOWN)
            {
                pressed = keyEvent.key.keysym.sym;
            }
        }

        return pressed;
    }

    public bool IsKeyPressed(int key)
    {
        bool pressed = false;
        Sdl.SDL_PumpEvents();
        Sdl.SDL_Event evt;
        Sdl.SDL_PollEvent(out evt);
        int numKeys;
        byte[] keys = Sdl.SDL_GetKeyState(out numKeys);
        if (keys[key] == 1)
            pressed = true;
        return pressed;
    }

    public void UpdateScreen()
    {
        Sdl.SDL_Flip(screen);
    }

    public void ClearScreen()
    {
        Sdl.SDL_Rect source = new Sdl.SDL_Rect(0, 0, screenWidth, screenHeight);
        Sdl.SDL_FillRect(screen, ref source, 0);
    }


    // Alternate key definitions
    public static int KEY_SPC = Sdl.SDLK_SPACE;
    public static int KEY_A = Sdl.SDLK_a;
    public static int KEY_B = Sdl.SDLK_b;
    public static int KEY_C = Sdl.SDLK_c;
    public static int KEY_D = Sdl.SDLK_d;
    public static int KEY_E = Sdl.SDLK_e;
    public static int KEY_F = Sdl.SDLK_f;
    public static int KEY_G = Sdl.SDLK_g;
    public static int KEY_H = Sdl.SDLK_h;
    public static int KEY_I = Sdl.SDLK_i;
    public static int KEY_J = Sdl.SDLK_j;
    public static int KEY_K = Sdl.SDLK_k;
    public static int KEY_L = Sdl.SDLK_l;
    public static int KEY_M = Sdl.SDLK_m;
    public static int KEY_N = Sdl.SDLK_n;
    public static int KEY_O = Sdl.SDLK_o;
    public static int KEY_P = Sdl.SDLK_p;
    public static int KEY_Q = Sdl.SDLK_q;
    public static int KEY_R = Sdl.SDLK_r;
    public static int KEY_S = Sdl.SDLK_s;
    public static int KEY_T = Sdl.SDLK_t;
    public static int KEY_U = Sdl.SDLK_u;
    public static int KEY_V = Sdl.SDLK_v;
    public static int KEY_W = Sdl.SDLK_w;
    public static int KEY_X = Sdl.SDLK_x;
    public static int KEY_Y = Sdl.SDLK_y;
    public static int KEY_Z = Sdl.SDLK_z;
    public static int KEY_1 = Sdl.SDLK_1;
    public static int KEY_2 = Sdl.SDLK_2;
    public static int KEY_3 = Sdl.SDLK_3;
    public static int KEY_4 = Sdl.SDLK_4;
    public static int KEY_5 = Sdl.SDLK_5;
    public static int KEY_6 = Sdl.SDLK_6;
    public static int KEY_7 = Sdl.SDLK_7;
    public static int KEY_8 = Sdl.SDLK_8;
    public static int KEY_9 = Sdl.SDLK_9;
    public static int KEY_0 = Sdl.SDLK_0;
    public static int KEY_RETURN = Sdl.SDLK_RETURN;

}


