﻿
class GameOverScreen : Screen
{
    const string CREDITS_IMAGE = "imgs/gameover.png";
    const int CREDITS_IMAGE_WIDTH = 640;
    const int CREDITS_IMAGE_HEIGHT = 380;
    protected Image imgCredits;
    protected Font myFont;
    private string namePlayer;
    protected bool exit;

    public string NamePlayer
    {
        get
        {
            return namePlayer;
        }

        set
        {
            namePlayer = value;
        }
    }

    public GameOverScreen(Hardware hardware) : base(hardware)
   {
         imgCredits = new Image(CREDITS_IMAGE, CREDITS_IMAGE_WIDTH,
             CREDITS_IMAGE_HEIGHT);
        myFont = new Font(Hardware.GAME_FONT, 20);
        NamePlayer = " ";
        exit = false;
    }

    public override void Show()
    {
        /*
         * We will show this screen until the user presses the space
         */
        do
        {
            // Place image in the middle of the screen
            imgCredits.MoveTo(Hardware.DEFAULT_WIDTH / 2 - CREDITS_IMAGE_WIDTH / 2,
                0);
            hardware.DrawImage(imgCredits);
            hardware.WriteText("Enter your name      Space to continue",
          Hardware.DEFAULT_WIDTH / 2 - 250, Hardware.DEFAULT_HEIGHT * 2 / 3
          , 255, 255, 0, myFont);
            EnterKeys();
            hardware.WriteText(NamePlayer,
        Hardware.DEFAULT_WIDTH / 2 - 250, Hardware.DEFAULT_HEIGHT*3/4
        , 255, 255, 0, myFont);
            hardware.UpdateScreen();
        }
        while (!exit);
    }

    public void EnterKeys()
    {

        int key = hardware.KeyPressed();
        if (NamePlayer.Length < 6)
        {
            if (key == Hardware.KEY_A)
                NamePlayer += "A";
            else if (key == Hardware.KEY_B)
                NamePlayer += "B";
            else if (key == Hardware.KEY_C)
                NamePlayer += "C";
            else if (key == Hardware.KEY_D)
                NamePlayer += "D";
            else if (key == Hardware.KEY_E)
                NamePlayer += "E";
            else if (key == Hardware.KEY_F)
                NamePlayer += "F";
            else if (key == Hardware.KEY_G)
                NamePlayer += "G";
            else if (key == Hardware.KEY_H)
                NamePlayer += "H";
            else if (key == Hardware.KEY_I)
                NamePlayer += "I";
            else if (key == Hardware.KEY_J)
                NamePlayer += "J";
            else if (key == Hardware.KEY_K)
                NamePlayer += "K";
            else if (key == Hardware.KEY_L)
                NamePlayer += "L";
            else if (key == Hardware.KEY_M)
                NamePlayer += "M";
            else if (key == Hardware.KEY_N)
                NamePlayer += "N";
            else if (key == Hardware.KEY_O)
                NamePlayer += "O";
            else if (key == Hardware.KEY_P)
                NamePlayer += "P";
            else if (key == Hardware.KEY_Q)
                NamePlayer += "Q";
            else if (key == Hardware.KEY_R)
                NamePlayer += "R";
            else if (key == Hardware.KEY_S)
                NamePlayer += "S";
            else if (key == Hardware.KEY_T)
                NamePlayer += "T";
            else if (key == Hardware.KEY_U)
                NamePlayer += "U";
            else if (key == Hardware.KEY_V)
                NamePlayer += "V";
            else if (key == Hardware.KEY_W)
                NamePlayer += "W";
            else if (key == Hardware.KEY_X)
                NamePlayer += "X";
            else if (key == Hardware.KEY_Y)
                NamePlayer += "Y";
            else if (key == Hardware.KEY_Z)
                NamePlayer += "Z";
            else if (key == Hardware.KEY_SPACE)
                exit = true;
        }

        else if (key == Hardware.KEY_SPACE)
            exit = true;
    }

    public void Reset()
    {
        exit = false;
        NamePlayer = " ";
    }
}
    
