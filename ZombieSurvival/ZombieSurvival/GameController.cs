﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */
using System;


class GameController
{
    public void Start()
    {
        Hardware hardware = new Hardware(Hardware.DEFAULT_WIDTH,
            Hardware.DEFAULT_HEIGHT, 24, false);

        WelcomeScreen welcome = new WelcomeScreen(hardware);
        CreditsScreen credits = new CreditsScreen(hardware);
        GameOverScreen overscreen = new GameOverScreen(hardware);
        LevelController levelcontroller = new LevelController(hardware);

        do
        {

            hardware.ClearScreen();
            welcome.Show();
            
            if (!welcome.GetExit())
            {
                hardware.ClearScreen();
                levelcontroller.Show_Level();
                hardware.ClearScreen();

                if (levelcontroller.GetCharacter().IsDead())
                {
                    //Temporary tests
                    overscreen.Show();
                }
                //Check if the score of this game is a high score
                credits.CheckHighScore(levelcontroller.GetScore(),overscreen.NamePlayer);
                credits.Show();
                levelcontroller.ResetGame();
                overscreen.Reset();
            }
            
        } while (!welcome.GetExit());
    }
}
