﻿/*
 Antonio Defez Ibanez
 ies san vicente
This class stores all the information of the Shot */

using System;
public class Shot : Sprite
{
    public const string SHOT_IMAGE = "imgs/bullet1.png";
    public const int SHOT_IMAGE_WIDTH = 16;
    public const int SHOT_IMAGE_HEIGHT = 16;
    public const int SHOT_MOVEMENT_INCREMENT = 32;
    private bool checkCollsion;

    public bool CheckCollsion
    {
        get
        {
            return checkCollsion;
        }

        set
        {
            checkCollsion = value;
        }
    }

    public Shot(int direction, int x, int y) : base(SHOT_IMAGE, SHOT_IMAGE_WIDTH,
        SHOT_IMAGE_HEIGHT, x, y, SHOT_MOVEMENT_INCREMENT)
    {
        this.Direction = direction;
        CheckCollsion = false;
    }

    
    public void Move()
    {
        this.Move_Sprite(Direction);
    }

    /*When a bullet hits the map disappears*/
    public override  void AfterCollisionMap()
    {
        //temporal test
        CheckCollsion = true;
    }
}
