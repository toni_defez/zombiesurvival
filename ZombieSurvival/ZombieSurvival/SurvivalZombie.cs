﻿/*
 Antonio Defez Ibanez
 ies san vicente
 
 Zombie Survival game starter class
 */

class SurvivalZombie 
{
   static void Main(string[] args)
   {
       GameController controller = new GameController();
       controller.Start();

   }
}

