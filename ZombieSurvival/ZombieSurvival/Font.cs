﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */
/*Class that allows us to easily draw texts on screen*/
using System;
public class Font
{
    IntPtr fontType;

    public Font(string fileName, int fontSize)
    {
        fontType = Tao.Sdl.SdlTtf.TTF_OpenFont(fileName, fontSize);
        if (fontType == IntPtr.Zero)
        {
            Console.WriteLine("Font type not found");
            Environment.Exit(2);
        }
    }

    public IntPtr GetFontType()
    {
        return fontType;
    }
}

