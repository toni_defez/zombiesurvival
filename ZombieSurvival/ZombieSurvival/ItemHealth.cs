﻿/*
 Antonio defez
 ies san vicente
 Item type that increases player health
 */

public class ItemHealth:Item
{
    public const string ITEM_IMAGE = "imgs/health.png";

    public ItemHealth() : base(ITEM_IMAGE)
    {

    }

    public override void EffectItem(Character player)
    {
        
        base.EffectItem(player);
        player.Lives+= 1;
    }
}

