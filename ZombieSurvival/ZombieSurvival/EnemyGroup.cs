﻿/*Antonio defez

The class enemy group is used to group all the enemies of each level.
This class is used to draw the enemies on the map and to see how they 
interact with the map and the player
 */

using System;       //For temporary tests

class EnemyGroup:ListSprite
{
    protected Enemy[] enemies;

    private ItemList itemList;
    public  ItemList ItemList
    {
        get
        {
            return itemList;
        }

        set
        {
            itemList = value;
        }
    }

    public EnemyGroup(int size)
    {
        enemies = new Enemy[size];
        ItemList = new ItemList();
        for (int i = 0; i < size; i++)
            enemies[i] = new Enemy();
    }



    public override Sprite Get(int c_enemy)
    {
        return enemies[c_enemy];
    }

    public override int Count()
    {
        return enemies.Length;
    }

    public override void Set(Sprite otherSprite, int index)
    {
        enemies[index] = (Enemy)otherSprite;
    }


    public override void Draw(Hardware hardware)
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            if (!enemies[i].IsDead)
            {
                enemies[i].Draw(hardware);
            }

        }
        //draw items of enemies deads

        ItemList.Draw(hardware);
    }
    /*
     * All enemies will move to the position of the character
     */
    public void Move(Character character_tofollow, int level_width, int level_height)
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            if (!enemies[i].IsDead)
            {
                enemies[i].Move(character_tofollow, level_width, level_height);
            }
           
        }

    }

    /*
     * Check if the character collides with an item in the list
     */
    public void ItemEnemyCollidesCharacter(Character Player)
    {
        for (int i = 0; i < itemList.Count(); i++)
        {
            if (Player.CollidesWith(itemList.Get(i),
                                            Character.CHARACTER_IMAGE_WIDTH,
                                            Character.CHARACTER_IMAGE_HEIGHT,
                                            Item.ITEM_IMAGE_WIDTH,
                                            Item.ITEM_IMAGE_HEIGHT))
            {
                  
                Item current_item = (Item)itemList.Get(i);
                current_item.EffectItem(Player);
            }
        }
    }


    /*Check if any enemy has hit a character also check  if the character 
     take a item*/
    public void EnemyGroupCollides(Character Player)
    {
        //comprobe if the character is touch by enemy
        for (int i = 0; i < enemies.Length; i++)
        {
            if (!enemies[i].IsDead && Player.CollidesWith(enemies[i], 
                                            Character.CHARACTER_IMAGE_WIDTH,
                                            Character.CHARACTER_IMAGE_HEIGHT,
                                             Enemy.ENEMY_IMAGE_WIDTH,
                                             Enemy.ENEMY_IMAGE_HEIGHT))
            {
                Player.TakingDamage(); 
            }
        }

        //Comprobe if the character touch a item;
        ItemEnemyCollidesCharacter(Player);
    }

    public void EnemyGroupShoots()
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            if (!enemies[i].IsDead && enemies[i].getShotList() != null)
                enemies[i].getShotList().MoveShoot();
        }
    }
    /*Check if a bullet collides with an enemy of EnemyGroup
    If there is a collision the enemy will be removed and the bullet will
    disappear
    */
    public void BulletCollision(Character player)
    {
        ShotList characterShotList = player.GetShotList();
        for(int c_enemy=0; c_enemy < enemies.Length;c_enemy++)
        {
            if (!enemies[c_enemy].IsDead)
            {
                for (int c_shot = 0; c_shot < characterShotList.Count(); c_shot++)
                {
                    if (enemies[c_enemy].CollidesWith(characterShotList.Get(c_shot),
                                                    Enemy.ENEMY_IMAGE_WIDTH,
                                                    Enemy.ENEMY_IMAGE_HEIGHT,
                                                    Shot.SHOT_IMAGE_WIDTH,
                                                    Shot.SHOT_IMAGE_HEIGHT))
                    {
                        enemies[c_enemy].AfterCollisionBullet();
                        player.Score += 10;
                        characterShotList.Get(c_shot).AfterCollisionMap();

                        /*We check if the enemy has died, if it is dead an 
                         * item appears in its position*/
                        if (enemies[c_enemy].IsDead)
                            ItemList.AddNewItem(enemies[c_enemy]);
                        
                    }
                }
            } 
        }

    }



    //This method checks the contents of the map curretly loaded and places the
    //enemies in the correct tile
    public void AddGroupToMap(Map map)
    {
        Block[,] array = map.GetArray();
        bool insert;
        for ( int i = 0; i < enemies.Length; i++)
        {
            insert = false;

            //We scour the entire map until we find a block in which the enemy
            //can be inserted
            foreach (Block block in array)
            {
                //One enemy one block 
                if (block.IsEnemy && !insert)
                {

                    //Depending on the type of index you have, you will create 
                    //a FixedEnemy or a MovingEnemy
                    if (block.Index == FixedEnemy.BLOCK_ID)
                    {
                        enemies[i] = new FixedEnemy();
                    }
                 
                    else if (block.Index == MovingEnemy.BLOCK_ID)
                    {
                        enemies[i] = new MovingEnemy();
                    }

                  //Positioning the enemy in the place of the block
                   enemies[i].getImage().MoveTo(
                                    (short)block.GetX(),
                                    (short)(block.GetY())
                                     );
                    block.IsEnemy=false;
                    insert = true;
                }
            }
        }
    }
}

