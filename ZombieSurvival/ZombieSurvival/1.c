﻿/** 
*  Antonio Defez
*  
*  Audio.cs 
*  This class is responsible for sound management
*  V 0.001 ->Centipe's version of Dam proyect
*/

using System;
using System.Collections.Generic;
using Tao.Sdl;


public class Audio
{
    List<IntPtr> audios;
    int channels;

    public Audio(int freq, int channels, int bytesPerSample)
    {
        this.channels = channels;
        SdlMixer.Mix_OpenAudio(freq, (short)SdlMixer.MIX_DEFAULT_FORMAT, channels,
                                bytesPerSample);
        audios = new List<IntPtr>();
    }

    public bool AddWAV(string fileName)
    {
        IntPtr file = SdlMixer.Mix_LoadWAV(fileName);
        if (file == IntPtr.Zero)
            return false;
        audios.Add(file);
        return true;
    }

    public void PlayWAV(int pos, int channel, int numberOfLoops)
    {
        if (pos >= 0 && pos < audios.Count && channel >= 1 && channel <= channels)
            SdlMixer.Mix_PlayChannel(channel, audios[pos], numberOfLoops);
    }

    public bool AddMusic(string fileName)
    {
        IntPtr file = SdlMixer.Mix_LoadMUS(fileName);
        if (file == IntPtr.Zero)
            return false;
        audios.Add(file);
        return true;
    }

    public void PlayMusic(int pos, int numberOfLoops)
    {
        if (pos >= 0 && pos < audios.Count)
            SdlMixer.Mix_PlayMusic(audios[pos], numberOfLoops);
    }

    public void StopMusic()
    {
        SdlMixer.Mix_HaltMusic();
    }

    public void StopChannel(int channel)
    {
        SdlMixer.Mix_HaltChannel(channel);
    }

}

﻿/*
Antonio Defez Ibanez
ies san vicente
This class creates the basic unit for map formation that is the block.
*/

public class Block : Image
{
    private bool isEnemy;
    public const int COLLISION_ID= 11;
    public const int FINAL_ID = 6;
    public  const short WIDTH_BLOCK= 32;
    public const short HEIGHT_BLOCK = 32;
    private int index;                   
    private bool collision_check;         //check if we can collide with it
    public  bool Collision_check
    {
        get
        {
            return collision_check;
        }

        set
        {
            collision_check = value;
        }
    }
    public  int Index
    {
        get
        {
            return index;
        }

        set
        {
            index = value;
        }
    }
    public bool IsEnemy
    {
        get
        {
            return isEnemy;
        }

        set
        {
            isEnemy = value;
        }
    }

    public bool IsFinal
    {
        get
        {
            return isFinal;
        }

        set
        {
            isFinal = value;
        }
    }

    private bool isFinal;


    public Block(string fileName, bool collision,int index,bool final) :
        base(fileName, WIDTH_BLOCK,HEIGHT_BLOCK)
    {
        this.Index = index;
        this.IsFinal = final;
        this.collision_check = collision;
    }
}

﻿/*
 Antonio Defez Ibanez
 ies san vicente
 This class will store all the information and instructions for
 the main character
 */

public class Character : Sprite
{
    public const string CHARACTER_IMAGE = "imgs/character1.png";
    public const int CHARACTER_IMAGE_WIDTH = 32;
    public const int CHARACTER_IMAGE_HEIGHT = 32;
    private int lives;
    private int score;
    protected ShotList characterShotList;
    private int bulletCounter;



    public int Lives
    {
        get
        {
            return lives;
        }

        set
        {
            lives = value;
        }
    }

    public  int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
        }
    }

    public int BulletCounter
    {
        get
        {
            return bulletCounter;
        }

        set
        {
            bulletCounter = value;
        }
    }

    public Character() : base(CHARACTER_IMAGE, CHARACTER_IMAGE_WIDTH, 
        CHARACTER_IMAGE_HEIGHT)
    {
        lives = 4;
        Movement_increment = 5;
        score = 0;
        TimeElapse = 0.5;
        characterShotList = new ShotList(TimeElapse);
        BulletCounter = 10;
    }

    public override void Draw(Hardware hardware)
    {
        base.Draw(hardware);
        characterShotList.Draw(hardware);
    }

    /*We add a new shot to the list*/
    public void AddShoot()
    {
        if (BulletCounter != 0 && characterShotList.CanShoot())
        {
            Shot current_shot = new Shot(Direction, this.GetX(), this.GetY());
            characterShotList.Add(current_shot);
            BulletCounter--;
        }
    }

    public ShotList GetShotList()
    {
        return characterShotList;
    }

    //Whenever the character takes damage it will lose a life
    public void TakingDamage()
    {
        lives--;
    }

    public bool IsDead()
    {
        return lives <= 0;
    }

    /*
     * I check the collisions of the bullets of the enemies with
     *  the sprite character, if it collides it subtracts a life
     */
    public void BulletCollision(Enemy enemy)
    {
        ShotList enemyShotList = enemy.getShotList();
        for (int c_shot = 0; c_shot < enemyShotList.Count(); c_shot++)
        {
            if (this.CollidesWith(enemyShotList.Get(c_shot),
                                            Character.CHARACTER_IMAGE_WIDTH,
                                            Character.CHARACTER_IMAGE_HEIGHT,
                                            Shot.SHOT_IMAGE_WIDTH,
                                            Shot.SHOT_IMAGE_HEIGHT))
            {
                this.TakingDamage();
                enemyShotList.Get(c_shot).AfterCollisionMap();
            }
        }
    }
}

﻿/*
 Antonio Defez Ibanez
 Ies san vicente

 Changelog: 16/05/2017
 Created a struct with the name and the score of
 the best players. Then it is stored in an array
 of high scores, that display the best 5 players'
 name and highscore
 */

using System;
using System.IO;


public class CreditsScreen : Screen
{
    public struct HighScore
    {
        public int Score;
        public string Name;
    }

    const string CREDITS_IMAGE = "imgs/credits.png";
    //Path of the text file that stores the high scores
    const string HIGH_SCORE_FILE = "highscores.sav";
    const int CREDITS_IMAGE_WIDTH = 318;
    const int CREDITS_IMAGE_HEIGHT = 192;
    const int NUM_OF_HIGH_SCORES = 5;
    //Array of structs that stores the High Scores
    protected HighScore[] highScores;
    protected Image imgCredits;
    protected Font myFont;

    public CreditsScreen(Hardware hardware) : base(hardware)
    {
        highScores = new HighScore[NUM_OF_HIGH_SCORES];
        imgCredits = new Image(CREDITS_IMAGE, CREDITS_IMAGE_WIDTH, 
                                    CREDITS_IMAGE_HEIGHT);
        myFont = new Font(Hardware.GAME_FONT, 20);
        Load();
    }

    public override void Show()
    {
        Load();
        do
        {
            hardware.ClearScreen();
            //Writing the title of the screen
            hardware.WriteText("High scores: ", Hardware.DEFAULT_WIDTH / 2 - 200,
                                Hardware.DEFAULT_HEIGHT - 500,
                                255, 255, 0, myFont);
            for (int i = 0; i < NUM_OF_HIGH_SCORES; i++)
            {
                //Printing each of the high scores
                hardware.WriteText(highScores[i].Name,
                    Hardware.DEFAULT_WIDTH / 2 - 200,
                    (short)(Hardware.DEFAULT_HEIGHT / 2 - 40 * i), 255, 255, 0,
                    myFont);
                hardware.WriteText(highScores[i].Score.ToString("D5"),
                    Hardware.DEFAULT_WIDTH / 2 + 50,
                    (short)(Hardware.DEFAULT_HEIGHT / 2 - 40 * i), 255, 255, 0,
                    myFont);

               
            }
            hardware.UpdateScreen();

            System.Threading.Thread.Sleep(10);
        }
        while (hardware.KeyPressed() != Hardware.KEY_SPACE); 
    }

    /* CheckHighScore method to be called when the game ends, to check if the score is higher
    than the previously saved high scores. If it is higher, then it enters to the 
    high scores array, that is resorted from higher to lower. */
    public void CheckHighScore(int score, string name)
    {
        bool highScoreFound = false;
        for (int i = 0; i < NUM_OF_HIGH_SCORES && !highScoreFound; i++)
        {
            if (score > highScores[i].Score)
            {
                highScores[i].Name = name;
                highScores[i].Score = score;
                highScoreFound = true;
            }
        }
        Sort();
        Save();
    }

    /* Bubblesort to sort high scores (Array.Sort won't work right
    this time) */
    public void Sort()
    {
        for (int i = 0; i < NUM_OF_HIGH_SCORES - 1; i++)
        {
            for (int j = i + 1; j < NUM_OF_HIGH_SCORES; j++)
            {
                if (highScores[i].Score > highScores[j].Score)
                {
                    HighScore temp = highScores[i];
                    highScores[i] = highScores[j];
                    highScores[j] = temp;
                }
            }
        }
    }

    public void Save()
    {
        try
        {
            StreamWriter writer = File.CreateText(HIGH_SCORE_FILE);
            foreach (HighScore highScore in highScores)
            {
                writer.WriteLine(highScore.Name + ";" + highScore.Score);
               
            }
            writer.Close();
        }
        catch (PathTooLongException)
        {
            Console.WriteLine("Error: PathTooLongException");
        }
        catch (IOException e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: "+e.Message);
        }
    }

   

    public void Load()
    {
        try
        {
            StreamReader reader = File.OpenText(HIGH_SCORE_FILE);
            for (int i = 0; i < NUM_OF_HIGH_SCORES; i++)
            {
                string highScoreString = reader.ReadLine();
                string[] highScoreArray = highScoreString.Split(';');
                highScores[i].Name = highScoreArray[0];
                highScores[i].Score = Convert.ToInt32(highScoreArray[1]);
            }
            reader.Close();
            Sort();
        }
        catch (PathTooLongException)
        {
            Console.WriteLine("Error: PathTooLongException");
        }
        catch (IOException e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
        }
    }
}
﻿/*
Antonio Defez Ibanez
ies san vicente
This class stores all the information of the Enemy class
 */
using System;

public class Enemy : Sprite
{
    protected bool isDead;
    public const int ENEMY_IMAGE_WIDTH = 32;
    public const int ENEMY_IMAGE_HEIGHT = 32;
    protected Audio audio;
    protected int numLives;
    public  bool IsDead
    {
        get
        {
            return isDead;
        }

        set
        {
            isDead = value;
        }
    }

    /*Empty builder for used in enemy group*/
    public Enemy():base()
    {
    }

    public Enemy(string img) : base(img, ENEMY_IMAGE_WIDTH, 
        ENEMY_IMAGE_HEIGHT)
    {
        isDead = false;
        audio = new Audio(44100, 2, 4096);
        numLives = 1;
    }

    //prevents Enemy from running across the map to get you
    public bool DistanceToObject(Sprite Object,int width,int height)
    {
        int totalx = this.GetX() - Object.GetX();
        int totaly = this.GetY() - Object.GetY();

        double total = Math.Sqrt(Math.Pow(totalx, 2) + Math.Pow(totaly, 2));

        return (total < width) || (total < height);
    }

    /*
     * FixedEnemy and MovingEnemy move differently
     */
    public virtual void Move(Character imgCharacter,int level_width,int level_height)
    {
        
    }

    public override void Draw(Hardware hardware)
    {
        base.Draw(hardware);
    }

    public override void AfterCollisionBullet()
    {
        numLives--;
        if(numLives<=0)
            isDead = true;
    }


    public virtual ShotList getShotList()
    {
        return null;
    }

}

﻿/*Antonio defez

The class enemy group is used to group all the enemies of each level.
This class is used to draw the enemies on the map and to see how they 
interact with the map and the player
 */

using System;       //For temporary tests

class EnemyGroup:ListSprite
{
    protected Enemy[] enemies;

    private ItemList itemList;
    public  ItemList ItemList
    {
        get
        {
            return itemList;
        }

        set
        {
            itemList = value;
        }
    }

    public EnemyGroup(int size)
    {
        enemies = new Enemy[size];
        ItemList = new ItemList();
        for (int i = 0; i < size; i++)
            enemies[i] = new Enemy();
    }



    public override Sprite Get(int c_enemy)
    {
        return enemies[c_enemy];
    }

    public override int Count()
    {
        return enemies.Length;
    }

    public override void Set(Sprite otherSprite, int index)
    {
        enemies[index] = (Enemy)otherSprite;
    }


    public override void Draw(Hardware hardware)
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            if (!enemies[i].IsDead)
            {
                enemies[i].Draw(hardware);
            }

        }
        //draw items of enemies deads

        ItemList.Draw(hardware);
    }
    /*
     * All enemies will move to the position of the character
     */
    public void Move(Character character_tofollow, int level_width, int level_height)
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            if (!enemies[i].IsDead)
            {
                enemies[i].Move(character_tofollow, level_width, level_height);
            }
           
        }

    }

    /*
     * Check if the character collides with an item in the list
     */
    public void ItemEnemyCollidesCharacter(Character Player)
    {
        for (int i = 0; i < itemList.Count(); i++)
        {
            if (Player.CollidesWith(itemList.Get(i),
                                            Character.CHARACTER_IMAGE_WIDTH,
                                            Character.CHARACTER_IMAGE_HEIGHT,
                                            Item.ITEM_IMAGE_WIDTH,
                                            Item.ITEM_IMAGE_HEIGHT))
            {
                  
                Item current_item = (Item)itemList.Get(i);
                current_item.EffectItem(Player);
            }
        }
    }


    /*Check if any enemy has hit a character also check  if the character 
     take a item*/
    public void EnemyGroupCollides(Character Player)
    {
        //comprobe if the character is touch by enemy
        for (int i = 0; i < enemies.Length; i++)
        {
            if (!enemies[i].IsDead && Player.CollidesWith(enemies[i], 
                                            Character.CHARACTER_IMAGE_WIDTH,
                                            Character.CHARACTER_IMAGE_HEIGHT,
                                             Enemy.ENEMY_IMAGE_WIDTH,
                                             Enemy.ENEMY_IMAGE_HEIGHT))
            {
                Player.TakingDamage(); 
            }
        }

        //Comprobe if the character touch a item;
        ItemEnemyCollidesCharacter(Player);
    }

    public void EnemyGroupShoots()
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            if (!enemies[i].IsDead && enemies[i].getShotList() != null)
                enemies[i].getShotList().MoveShoot();
        }
    }
    /*Check if a bullet collides with an enemy of EnemyGroup
    If there is a collision the enemy will be removed and the bullet will
    disappear
    */
    public void BulletCollision(Character player)
    {
        ShotList characterShotList = player.GetShotList();
        for(int c_enemy=0; c_enemy < enemies.Length;c_enemy++)
        {
            if (!enemies[c_enemy].IsDead)
            {
                for (int c_shot = 0; c_shot < characterShotList.Count(); c_shot++)
                {
                    if (enemies[c_enemy].CollidesWith(characterShotList.Get(c_shot),
                                                    Enemy.ENEMY_IMAGE_WIDTH,
                                                    Enemy.ENEMY_IMAGE_HEIGHT,
                                                    Shot.SHOT_IMAGE_WIDTH,
                                                    Shot.SHOT_IMAGE_HEIGHT))
                    {
                        enemies[c_enemy].AfterCollisionBullet();
                        player.Score += 10;
                        characterShotList.Get(c_shot).AfterCollisionMap();

                        /*We check if the enemy has died, if it is dead an 
                         * item appears in its position*/
                        if (enemies[c_enemy].IsDead)
                            ItemList.AddNewItem(enemies[c_enemy]);
                        
                    }
                }
            } 
        }

    }



    //This method checks the contents of the map curretly loaded and places the
    //enemies in the correct tile
    public void AddGroupToMap(Map map)
    {
        Block[,] array = map.GetArray();
        bool insert;
        for ( int i = 0; i < enemies.Length; i++)
        {
            insert = false;

            //We scour the entire map until we find a block in which the enemy
            //can be inserted
            foreach (Block block in array)
            {
                //One enemy one block 
                if (block.IsEnemy && !insert)
                {

                    //Depending on the type of index you have, you will create 
                    //a FixedEnemy or a MovingEnemy
                    if (block.Index == FixedEnemy.BLOCK_ID)
                    {
                        enemies[i] = new FixedEnemy();
                    }
                 
                    else if (block.Index == MovingEnemy.BLOCK_ID)
                    {
                        enemies[i] = new MovingEnemy();
                    }

                  //Positioning the enemy in the place of the block
                   enemies[i].getImage().MoveTo(
                                    (short)block.GetX(),
                                    (short)(block.GetY())
                                     );
                    block.IsEnemy=false;
                    insert = true;
                }
            }
        }
    }
}

﻿/*
 Antonio Defez Ibanez
 ies san vicente
 This method checks the contents of the EnemyFixe , this enemy can't move
 */
using System;
class FixedEnemy : Enemy
{
    public const  string ENEMY_IMAGE_FIXED = "imgs/FixedEnemy.png";
    public  const  int BLOCK_ID=28;
    protected ShotList enemyShotList;
    protected Random r;

    public FixedEnemy() : base(ENEMY_IMAGE_FIXED)
    {
        //The base class has everything that we need, 
        //so we don't have to add or modify any additional data
        TimeElapse = 0.5;
        enemyShotList = new ShotList(TimeElapse);
       
        r = new Random();
    }

    /*
     * The fixed enemy will have a random behavior and shoot in vertical or 
     * horizontal depending on random factors
     */
    public override void Move(Character CharacterToFollow, int level_width, int level_height)
    {
        int key = -3;
        int behaviour = r.Next(0, 2);
        if (this.DistanceToObject(CharacterToFollow, level_width, level_height))
        {
            if (behaviour == 0)
            {
                if (CharacterToFollow.GetX() > this.GetX())
                    key = Sprite.MOVE_RIGHT;

                if (CharacterToFollow.GetX() < this.GetX())
                    key = Sprite.MOVE_LEFT;
            }

            else
            {
                if (CharacterToFollow.GetY() > this.GetY())
                    key = Sprite.MOVE_DOWN;
                else
                    key = Sprite.MOVE_UP;
            }
            Direction = key;
            AddShoot();
        }
    }


    public void AddShoot()
    {
        
        Shot current_shot = new Shot(Direction, this.GetX(), this.GetY());
        enemyShotList.Add(current_shot);
    }

    public override void Draw(Hardware hardware)
    {
        base.Draw(hardware);
        enemyShotList.Draw(hardware);
    }

    public override ShotList getShotList()
    {
        return enemyShotList;
    }
}

﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */
/*Class that allows us to easily draw texts on screen*/
using System;
public class Font
{
    IntPtr fontType;

    public Font(string fileName, int fontSize)
    {
        fontType = Tao.Sdl.SdlTtf.TTF_OpenFont(fileName, fontSize);
        if (fontType == IntPtr.Zero)
        {
            Console.WriteLine("Font type not found");
            Environment.Exit(2);
        }
    }

    public IntPtr GetFontType()
    {
        return fontType;
    }
}

﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */
using System;


class GameController
{
    public void Start()
    {
        Hardware hardware = new Hardware(Hardware.DEFAULT_WIDTH,
            Hardware.DEFAULT_HEIGHT, 24, false);

        WelcomeScreen welcome = new WelcomeScreen(hardware);
        CreditsScreen credits = new CreditsScreen(hardware);
        GameOverScreen overscreen = new GameOverScreen(hardware);
        LevelController levelcontroller = new LevelController(hardware);

        do
        {

            hardware.ClearScreen();
            welcome.Show();
            
            if (!welcome.GetExit())
            {
                hardware.ClearScreen();
                levelcontroller.Show_Level();
                hardware.ClearScreen();

                if (levelcontroller.GetCharacter().IsDead())
                {
                    //Temporary tests
                    overscreen.Show();
                }
                //Check if the score of this game is a high score
                credits.CheckHighScore(levelcontroller.GetScore(),overscreen.NamePlayer);
                credits.Show();
                levelcontroller.ResetGame();
                overscreen.Reset();
            }
            
        } while (!welcome.GetExit());
    }
}
﻿
class GameOverScreen : Screen
{
    const string CREDITS_IMAGE = "imgs/gameover.png";
    const int CREDITS_IMAGE_WIDTH = 640;
    const int CREDITS_IMAGE_HEIGHT = 380;
    protected Image imgCredits;
    protected Font myFont;
    private string namePlayer;
    protected bool exit;

    public string NamePlayer
    {
        get
        {
            return namePlayer;
        }

        set
        {
            namePlayer = value;
        }
    }

    public GameOverScreen(Hardware hardware) : base(hardware)
   {
         imgCredits = new Image(CREDITS_IMAGE, CREDITS_IMAGE_WIDTH,
             CREDITS_IMAGE_HEIGHT);
        myFont = new Font(Hardware.GAME_FONT, 20);
        NamePlayer = " ";
        exit = false;
    }

    public override void Show()
    {
        /*
         * We will show this screen until the user presses the space
         */
        do
        {
            // Place image in the middle of the screen
            imgCredits.MoveTo(Hardware.DEFAULT_WIDTH / 2 - CREDITS_IMAGE_WIDTH / 2,
                0);
            hardware.DrawImage(imgCredits);
            hardware.WriteText("Enter your name      Space to continue",
          Hardware.DEFAULT_WIDTH / 2 - 250, Hardware.DEFAULT_HEIGHT * 2 / 3
          , 255, 255, 0, myFont);
            EnterKeys();
            hardware.WriteText(NamePlayer,
        Hardware.DEFAULT_WIDTH / 2 - 250, Hardware.DEFAULT_HEIGHT*3/4
        , 255, 255, 0, myFont);
            hardware.UpdateScreen();
        }
        while (!exit);
    }

    public void EnterKeys()
    {

        int key = hardware.KeyPressed();
        if (NamePlayer.Length < 6)
        {
            if (key == Hardware.KEY_A)
                NamePlayer += "A";
            else if (key == Hardware.KEY_B)
                NamePlayer += "B";
            else if (key == Hardware.KEY_C)
                NamePlayer += "C";
            else if (key == Hardware.KEY_D)
                NamePlayer += "D";
            else if (key == Hardware.KEY_E)
                NamePlayer += "E";
            else if (key == Hardware.KEY_F)
                NamePlayer += "F";
            else if (key == Hardware.KEY_G)
                NamePlayer += "G";
            else if (key == Hardware.KEY_H)
                NamePlayer += "H";
            else if (key == Hardware.KEY_I)
                NamePlayer += "I";
            else if (key == Hardware.KEY_J)
                NamePlayer += "J";
            else if (key == Hardware.KEY_K)
                NamePlayer += "K";
            else if (key == Hardware.KEY_L)
                NamePlayer += "L";
            else if (key == Hardware.KEY_M)
                NamePlayer += "M";
            else if (key == Hardware.KEY_N)
                NamePlayer += "N";
            else if (key == Hardware.KEY_O)
                NamePlayer += "O";
            else if (key == Hardware.KEY_P)
                NamePlayer += "P";
            else if (key == Hardware.KEY_Q)
                NamePlayer += "Q";
            else if (key == Hardware.KEY_R)
                NamePlayer += "R";
            else if (key == Hardware.KEY_S)
                NamePlayer += "S";
            else if (key == Hardware.KEY_T)
                NamePlayer += "T";
            else if (key == Hardware.KEY_U)
                NamePlayer += "U";
            else if (key == Hardware.KEY_V)
                NamePlayer += "V";
            else if (key == Hardware.KEY_W)
                NamePlayer += "W";
            else if (key == Hardware.KEY_X)
                NamePlayer += "X";
            else if (key == Hardware.KEY_Y)
                NamePlayer += "Y";
            else if (key == Hardware.KEY_Z)
                NamePlayer += "Z";
            else if (key == Hardware.KEY_SPACE)
                exit = true;
        }

        else if (key == Hardware.KEY_SPACE)
            exit = true;
    }

    public void Reset()
    {
        exit = false;
        NamePlayer = " ";
    }
}
    
﻿/*
 Antonio Defez
 
Shows the player interface in the game, in this interface 
will appear data such as current life, playing time and player's score
 */


public class Gui
{
    protected string direction_guy;
    protected string direction_heart;
    protected Image gui_principal;
    protected int gui_width;
    protected int gui_height;

    protected Image heart;
    protected int heart_width;
    protected int heart_height;

    public const int GUI_IMAGE_WIDTH = 799;
    public const int GUI_IMAGE_HEIGHT = 70;
    public const int HEART_IMAGE_WIDTH = 500;
    public const int HEART_IMAGE_HEIGHT = 500;

    private int score;
    public  int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
        }
    }

    public int Numhearts
    {
        get
        {
            return numhearts;
        }

        set
        {
            numhearts = value;
        }
    }

    public int Numshoot
    {
        get
        {
            return numshoot;
        }

        set
        {
            numshoot = value;
        }
    }

    private int numhearts;
    private int numshoot;
 
    public Gui(Character player)
    {
        this.direction_guy = "imgs/gui.png";
        this.direction_heart = "imgs/live.png";
        this.gui_height = GUI_IMAGE_WIDTH;
        this.gui_width = GUI_IMAGE_HEIGHT;
        this.gui_principal = new Image(this.direction_guy, GUI_IMAGE_WIDTH,
            GUI_IMAGE_HEIGHT);
        this.heart = new Image(direction_heart, HEART_IMAGE_WIDTH,
           HEART_IMAGE_HEIGHT);
        this.score = player.Score;
        this.numhearts = player.Lives;
        this.numshoot = player.BulletCounter;
        
    }

    public void Update(Character player)
    {
        this.score = player.Score;
        this.numhearts = player.Lives;
        this.numshoot = player.BulletCounter;
    }

    public void  Draw(Hardware hardware)
    {
        hardware.DrawImage(this.gui_principal);
        Font myFont = new Font(Hardware.GAME_FONT, 20);
        hardware.WriteText("HP", 10, 20,
            255, 255, 0, myFont);
        hardware.WriteText("Score", 10, 40,
            255, 255, 0, myFont);
        hardware.WriteText(this.score.ToString("0000"), 100, 40,
            255, 255, 0, myFont);
        hardware.WriteText(this.numshoot.ToString("0000"), 600, 20,
             255, 255, 0, myFont);

        for (int i = 0; i < numhearts; i++)
        {
            this.heart.MoveTo((short)(200 + i * 50), 20);
            hardware.DrawImage(this.heart);
        }
    }
}

﻿/*(c)Antonio Defez*/
/*
 * We will use this class as interface to speed up the creation of windows,
 * and avoid using SDL-TAO directly
 * To this class we will send the parameters of width, height, color palette 
 * and if this screen completes to create the screen
 * 
 * changes:
 * 0.1 Initial version, based on Proyect Centipede 
 * 0.11 , 14-may-2017: Fixed Bug in WriteHiddenText
 * 
 */
using System;
using Tao.Sdl;

public class Hardware
{
    public const int DEFAULT_WIDTH = 800;
    public const int DEFAULT_HEIGHT = 600;

    public const string GAME_FONT = "resources/DAYPBL__.ttf";

    public static int KEY_ESC = Sdl.SDLK_ESCAPE;
    public static int KEY_UP = Sdl.SDLK_UP;
    public static int KEY_DOWN = Sdl.SDLK_DOWN;
    public static int KEY_LEFT = Sdl.SDLK_LEFT;
    public static int KEY_RIGHT = Sdl.SDLK_RIGHT;
    public static int KEY_SPACE = Sdl.SDLK_SPACE;

    short screenWidth;
    short screenHeight;
    short colorDepth;
    IntPtr screen;

    public Hardware(short width, short height, short depth, bool fullScreen)
    {
        screenWidth = width;
        screenHeight = height;
        colorDepth = depth;

        int flags = Sdl.SDL_HWSURFACE | Sdl.SDL_DOUBLEBUF | Sdl.SDL_ANYFORMAT;
        if (fullScreen)
            flags = flags | Sdl.SDL_FULLSCREEN;

        Sdl.SDL_Init(Sdl.SDL_INIT_EVERYTHING);
        screen = Sdl.SDL_SetVideoMode(screenWidth, screenHeight, colorDepth, flags);
        Sdl.SDL_Rect rect = new Sdl.SDL_Rect(0, 0, screenWidth, screenHeight);
        Sdl.SDL_SetClipRect(screen, ref rect);

        SdlTtf.TTF_Init();
    }

    ~Hardware()
    {
        Sdl.SDL_Quit();
    }

    public void DrawImage(Image img)
    {
        Sdl.SDL_Rect source = new Sdl.SDL_Rect(0, 0, img.GetImageWidth(),
            img.GetImageHeight());
        Sdl.SDL_Rect target = new Sdl.SDL_Rect(img.GetX(), img.GetY(), 
            img.GetImageWidth(), img.GetImageHeight());
        Sdl.SDL_BlitSurface(img.GetImage(), ref source, screen, ref target);
    }
    public void DrawImage(Image img, int x, int y)
    {
        Sdl.SDL_Rect source = new Sdl.SDL_Rect((short)x, (short)y,
            img.GetImageWidth(), img.GetImageHeight());
        Sdl.SDL_Rect target = new Sdl.SDL_Rect(img.GetX(), img.GetY(),
            img.GetImageWidth(), img.GetImageHeight());
        Sdl.SDL_BlitSurface(img.GetImage(), ref source, screen, ref target);
    }

    public void DrawImage(Image image, short x, short y, short width, short height)
    {
        Sdl.SDL_Rect src = new Sdl.SDL_Rect(x, y, width, height);
        Sdl.SDL_Rect dest = new Sdl.SDL_Rect(image.GetX(), image.GetY(),
            width, height);
        Sdl.SDL_BlitSurface(image.GetImage(), ref src, screen, ref dest);
    }

    public void WriteText(string text, short x, short y,
                                    byte r, byte g, byte b, Font fontType)
    {
        Sdl.SDL_Color color = new Sdl.SDL_Color(r, g, b);
        IntPtr textAsImage = SdlTtf.TTF_RenderText_Solid(fontType.GetFontType(),
            text, color);
        if (textAsImage == IntPtr.Zero)
            Environment.Exit(5);
        Sdl.SDL_Rect src = new Sdl.SDL_Rect(0, 0, screenWidth, screenHeight);
        Sdl.SDL_Rect dest = new Sdl.SDL_Rect(x, y, screenWidth, screenHeight);
        Sdl.SDL_BlitSurface(textAsImage, ref src, screen, ref dest);
        Sdl.SDL_FreeSurface(textAsImage);
    }

    public int KeyPressed()
    {
        int pressed = -1;

        Sdl.SDL_PumpEvents();
        Sdl.SDL_Event keyEvent;
        if (Sdl.SDL_PollEvent(out keyEvent) == 1)
        {
            if (keyEvent.type == Sdl.SDL_KEYDOWN)
            {
                pressed = keyEvent.key.keysym.sym;
            }
        }

        return pressed;
    }

    public bool IsKeyPressed(int key)
    {
        bool pressed = false;
        Sdl.SDL_PumpEvents();
        Sdl.SDL_Event evt;
        Sdl.SDL_PollEvent(out evt);
        int numKeys;
        byte[] keys = Sdl.SDL_GetKeyState(out numKeys);
        if (keys[key] == 1)
            pressed = true;
        return pressed;
    }

    public void UpdateScreen()
    {
        Sdl.SDL_Flip(screen);
    }

    public void ClearScreen()
    {
        Sdl.SDL_Rect source = new Sdl.SDL_Rect(0, 0, screenWidth, screenHeight);
        Sdl.SDL_FillRect(screen, ref source, 0);
    }


    // Alternate key definitions
    public static int KEY_SPC = Sdl.SDLK_SPACE;
    public static int KEY_A = Sdl.SDLK_a;
    public static int KEY_B = Sdl.SDLK_b;
    public static int KEY_C = Sdl.SDLK_c;
    public static int KEY_D = Sdl.SDLK_d;
    public static int KEY_E = Sdl.SDLK_e;
    public static int KEY_F = Sdl.SDLK_f;
    public static int KEY_G = Sdl.SDLK_g;
    public static int KEY_H = Sdl.SDLK_h;
    public static int KEY_I = Sdl.SDLK_i;
    public static int KEY_J = Sdl.SDLK_j;
    public static int KEY_K = Sdl.SDLK_k;
    public static int KEY_L = Sdl.SDLK_l;
    public static int KEY_M = Sdl.SDLK_m;
    public static int KEY_N = Sdl.SDLK_n;
    public static int KEY_O = Sdl.SDLK_o;
    public static int KEY_P = Sdl.SDLK_p;
    public static int KEY_Q = Sdl.SDLK_q;
    public static int KEY_R = Sdl.SDLK_r;
    public static int KEY_S = Sdl.SDLK_s;
    public static int KEY_T = Sdl.SDLK_t;
    public static int KEY_U = Sdl.SDLK_u;
    public static int KEY_V = Sdl.SDLK_v;
    public static int KEY_W = Sdl.SDLK_w;
    public static int KEY_X = Sdl.SDLK_x;
    public static int KEY_Y = Sdl.SDLK_y;
    public static int KEY_Z = Sdl.SDLK_z;
    public static int KEY_1 = Sdl.SDLK_1;
    public static int KEY_2 = Sdl.SDLK_2;
    public static int KEY_3 = Sdl.SDLK_3;
    public static int KEY_4 = Sdl.SDLK_4;
    public static int KEY_5 = Sdl.SDLK_5;
    public static int KEY_6 = Sdl.SDLK_6;
    public static int KEY_7 = Sdl.SDLK_7;
    public static int KEY_8 = Sdl.SDLK_8;
    public static int KEY_9 = Sdl.SDLK_9;
    public static int KEY_0 = Sdl.SDLK_0;
    public static int KEY_RETURN = Sdl.SDLK_RETURN;

}


﻿/*(c)Antonio Defez
 * We will use this class as interface to speed up the creation of imagens,
 * and avoid using SDL-TAO directly
 * To this class we will send the parameters of width, height, direction_image
 */ 
using System;
using Tao.Sdl;


public class Image
{
 
    protected short x, y;
    protected short imageWidth, imageHeight;
    protected IntPtr image;

    public Image(string fileName, short width, short height)
    {
        image = SdlImage.IMG_Load(fileName);
  
        if (image == IntPtr.Zero)
        {

            Console.WriteLine("Image not found");
            Environment.Exit(1);
        }
        imageWidth = width;
        imageHeight = height;
    }

    public void MoveTo(short x, short y)
    {
        this.x = x;
        this.y = y;
    }

    public short GetX()
    {
        return x;
    }

    public short GetY()
    {
        return y;
    }

    public void SetX(short x)
    {
        this.x = x;
    }

    public void SetY(short y)
    {
        this.y = y;
    }

    public short GetImageWidth()
    {
        return imageWidth;
    }

    public short GetImageHeight()
    {
        return imageHeight;
    }

    public IntPtr GetImage()
    {
        return image;
    }

    public virtual bool CollidesWith(Image img, short w1, short h1, short w2, short h2)
    {
        return (x + w1 >= img.x && x <= img.x + w2 &&
                y + h1 >= img.y && y <= img.y + h2);
    }

}﻿
using System;
public class Item : Sprite
{
    public const int ITEM_IMAGE_WIDTH = 32;
    public const int ITEM_IMAGE_HEIGHT = 32;
    private bool checkCollsion;

    public bool CheckCollsion
    {
        get
        {
            return checkCollsion;
        }

        set
        {
            checkCollsion = value;
        }
    }

    public Item(string item_image) : base(item_image, ITEM_IMAGE_WIDTH,
        ITEM_IMAGE_HEIGHT)
    {
        checkCollsion = false;
    }


    public virtual   void EffectItem(Character player)
    {
        CheckCollsion = true;
        
    }
}

﻿
public class ItemBullet : Item
{
    public const string ITEM_IMAGE = "imgs/health.png";

    public ItemBullet() : base(ITEM_IMAGE)
    {

    }

    public override void EffectItem(Character player)
    {

        base.EffectItem(player);
        player.BulletCounter += 10;
    }
}
﻿
public class ItemHealth:Item
{
    public const string ITEM_IMAGE = "imgs/health.png";

    public ItemHealth() : base(ITEM_IMAGE)
    {

    }

    public override void EffectItem(Character player)
    {
        
        base.EffectItem(player);
        player.Lives+= 1;
    }
}

﻿
using System.Collections.Generic;
using System;
public class ItemList:ListSprite
{
    protected List<Item> items;
    private Random r_item;
    public ItemList()
    {
        items = new List<Item>();
        r_item = new Random();
    }

    public override void Add(Sprite otherSprite)
    {
        items.Add((Item)otherSprite);
    }

    public override int   Count()
    {
        return items.Count;
    }

    //TODO THIS CODE IS EXACLY THE SAME OF LISTSHOOT
    //IT MUST BE HERATY OF SHOOT ?
    //
    public override Sprite Get(int cont)
    {
        return (Item)items[cont];
    }

    public void  AddNewItem(Sprite denemy)
    {
        /*
         * Usamos random y creamos un objeto al azar
         * por ahora podemos meter varios objetos
         * uno para la vida, otro para la velocidad*/
        int number_item = r_item.Next(0, 6);
        Item nexItem = null;
        switch (number_item)
        {
            case 0://Item healt
                nexItem=new ItemHealth();
                break;
            case 1://Item Velocity
                nexItem = new ItemSpeed();
                break;
            case 2://Item money
                nexItem = new ItemMoney();
                break;
            case 3: //item QuickScope
                nexItem = new ItemQuickShoot();
                break;
            case 4:
            case 5:
                nexItem = new ItemBullet();
                break;
        }
        nexItem.getImage().MoveTo(
                                    denemy.GetX(),
                                    denemy.GetY()
                                     );
                                     
        this.Add(nexItem);
    }

    public override void Draw(Hardware hardware)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].CheckCollsion)
            {
                items.Remove(items[i]);
            }
            else
            {
                items[i].Draw(hardware);
            }
        }
    }

}

﻿
public class ItemMoney : Item
{
    public const string ITEM_IMAGE = "imgs/money.png";

    public ItemMoney() : base(ITEM_IMAGE)
    {

    }

    public override void EffectItem(Character player)
    {

        base.EffectItem(player);
        player.Score += 100;
    }
}
﻿public class ItemQuickShoot : Item
{
    public const string ITEM_IMAGE = "imgs/quickshoot.png";

    public ItemQuickShoot() : base(ITEM_IMAGE)
    {

    }

    public override void EffectItem(Character player)
    {

        base.EffectItem(player);
        player.TimeElapse += 0.1;
    }
}
﻿
public class ItemSpeed : Item
{
    public const string ITEM_IMAGE = "imgs/fast.png";

    public ItemSpeed() : base(ITEM_IMAGE)
    {

    }

    public override void EffectItem(Character player)
    {

        base.EffectItem(player);
        player.Movement_increment += 1;
    }
}
﻿/*
 Antonio Defez Ibanez
 ies san vicente
 
Each level is a screen, in this screen we will find all elemntos of the game.
The map, enemies, player.
In this class there is also the game loop.
And the gui structure that defines the player's gui
Change 16/05/2017: Added call to CheckScore method from
CreditsScreen
 */


class Level:Screen
{
    //const for level
    const int LEVEL_IMAGE_WIDTH = 318;
    const int LEVEL_IMAGE_HEIGHT = 192;

    protected bool finished;
    protected int index;
    protected Map mapLevel;


    protected Character player;
    protected EnemyGroup enemiesGroup;
    protected Gui guiPlayer;
    protected int increment_scroll;

    protected Audio audio;

    public Level(Hardware hardware, int index,
        ref Character  MainCharacter) : base(hardware)
    {
        this.index = index;
        this.mapLevel = new Map(index);
        finished = false;
        player = MainCharacter;
        guiPlayer = new Gui(player);
        enemiesGroup = new EnemyGroup(mapLevel.GetTotalEnemy());
        //Put all enemies on the map
        enemiesGroup.AddGroupToMap(mapLevel);
        increment_scroll = 5;
        audio = new Audio(44100, 2, 4096);
    }

    public override void Show()
    {
        
        Font myFont = new Font(Hardware.GAME_FONT, 20);
        player.getImage().MoveTo(200, 200);
        audio.AddWAV("music/mario_bump.wav");
        audio.AddMusic("music/"+index+".mid");

        audio.PlayMusic(1, -1);

        while (!finished && !player.IsDead() )
        {
            // 1. Draw everything
            Draw();
            //2.Chek keys
            CheckKeys();
            // 3. Move other elements (automatically)
            AutomaticMovement();
            
            // 4. Collision detection
            CheckCollision();
            // 5. Pause game
            System.Threading.Thread.Sleep(10);
        }
        finished = true;
    }

    public void AutomaticMovement()
    {
        enemiesGroup.Move(player, Level.LEVEL_IMAGE_WIDTH,
                Level.LEVEL_IMAGE_HEIGHT);
        //Move shots
        player.GetShotList().MoveShoot();
        //Move shotsenemy
        enemiesGroup.EnemyGroupShoots();

        guiPlayer.Update(player);
        //scroll enemies /shot /items
        mapLevel.LocatedSpritesScroll(player.GetShotList());
        mapLevel.LocatedSpritesScroll(enemiesGroup.ItemList);
        //move enemies
        mapLevel.LocatedSpritesScroll(enemiesGroup);
        //move shotlist of all enemies
        mapLevel.ScrollEnemyShoot(enemiesGroup,player);
        mapLevel.MoveMap();

    }

    public void Draw()
    {
        hardware.ClearScreen();
        mapLevel.Draw(hardware);
        enemiesGroup.Draw(hardware);
        player.Draw(hardware);
       
        guiPlayer.Draw(hardware);
        hardware.UpdateScreen();
    }

    /*Depending on where the character moves the map will scroll or will not do 
     * it.Compiling as long as you do not leave the map
     */
    public void Move_left()
    {
        if (mapLevel.TOTAL_X <= 0 && player.GetX() <= Hardware.DEFAULT_WIDTH / 2)
        {
            player.Direction=Character.MOVE_LEFT;
            player.UpdateAnim(Character.MOVE_LEFT);
            mapLevel.MAP_X += increment_scroll;
            mapLevel.TOTAL_X += increment_scroll;
        }
        else if (player.GetX() > 0)
        {
            player.Move_Sprite(Sprite.MOVE_LEFT);
        }
    }

    public void Move_right()
    {
        if (mapLevel.TOTAL_X >= -mapLevel.WIDTH_MAP + Hardware.DEFAULT_WIDTH
                 &&
                 player.GetX() >= Hardware.DEFAULT_WIDTH / 2)
        {
            player.Direction=Character.MOVE_RIGHT;
            player.UpdateAnim(Character.MOVE_RIGHT);

            mapLevel.MAP_X -= increment_scroll;
            mapLevel.TOTAL_X -= increment_scroll;
        }
        else if (player.GetX() < Hardware.DEFAULT_WIDTH - player.GetSpriteWidth())
        {
            player.Move_Sprite(Sprite.MOVE_RIGHT);
        }
    }

    public void Move_up()
    {     
        if (mapLevel.TOTAL_Y <=0 &&
               player.GetY() <= Hardware.DEFAULT_HEIGHT / 2)
        {
            player.Direction=Character.MOVE_UP;
            player.UpdateAnim(Character.MOVE_UP);
            CheckCollision();
            mapLevel.MAP_Y += increment_scroll;
            mapLevel.TOTAL_Y += increment_scroll;
        }
        else if (player.GetY() <= Hardware.DEFAULT_HEIGHT - player.GetSpriteHeight()
            && player.GetY() > 0)
        {
            player.Move_Sprite(Sprite.MOVE_UP);
        }
    }

    public void Move_down()
    {
        if ( mapLevel.TOTAL_Y >= -mapLevel.HEIGTH_MAP + Hardware.DEFAULT_HEIGHT
            && player.GetY() > Hardware.DEFAULT_HEIGHT / 2)
        {
            player.Direction=Character.MOVE_DOWN;
            player.UpdateAnim(Character.MOVE_DOWN);
            mapLevel.MAP_Y -= increment_scroll;
            mapLevel.TOTAL_Y -= increment_scroll;
        }
        else if (player.GetY() < Hardware.DEFAULT_HEIGHT - player.GetSpriteHeight())
        {
            player.Move_Sprite(Sprite.MOVE_DOWN);
        }
    }

    /*Check Keyboard Events*/
    public void CheckKeys()
    {
        increment_scroll = player.Movement_increment;
        if (hardware.IsKeyPressed(Hardware.KEY_LEFT))
            Move_left();

        else if (hardware.IsKeyPressed(Hardware.KEY_RIGHT))
        {
            Move_right();
        }
        else if (hardware.IsKeyPressed(Hardware.KEY_DOWN))
        {
            Move_down();
        }
        else if (hardware.IsKeyPressed(Hardware.KEY_UP))
        {
            Move_up();
        }

        if (hardware.IsKeyPressed(Hardware.KEY_SPACE))
        {
            audio.PlayWAV(0, 2, 0);
            player.AddShoot();
        }
    }

    public void CheckCollision()
    {
        //checks collision between enemy and playe
        enemiesGroup.EnemyGroupCollides(player);
        //check collision between player and map
        mapLevel.CheckCollision(player);
        //chek collision between enmies and map
        mapLevel.CheckCollisonGroup(enemiesGroup);
        //check collsion between player's shot and map
        mapLevel.CheckCollisonGroup(player.GetShotList());

        //check collision between enemeies and
        mapLevel.CheckCollisonGroup(enemiesGroup.ItemList);
        enemiesGroup.BulletCollision(player);

        //Check if we have collided with the final block,
        //if we collided with that block we have passed the level
        finished = mapLevel.IsFinished;
    }

    public void setFinished(bool state)
    {
        this.finished = state;
    }

    public bool getFinished()
    {
        return finished;
    }

    public Character getCharacter()
    {
        return player;
    }
}

﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */
/*
This class is responsible for iterating between the different levels and 
checking its status

The main character is the one that is sent to all levels and serves to keep
information regarding your score, total lives 
*/

using System;

class LevelController 
{
    protected bool exit;
    protected  Level[] levels;
    protected Character MainCharacter;
    protected int currentLevel;
    protected Hardware hardware;
    protected int score_total;

    public LevelController(Hardware hardware) 
    {

        levels = new Level[3];
        MainCharacter = new Character();
        this.hardware = hardware;
        exit = false;
        
    }

    public  void Show_Level()
    {
        currentLevel = 0;

        for (int i = 0; i < levels.Length && !MainCharacter.IsDead(); i++)
        {
            Console.WriteLine(i);
            levels[i] = new Level(hardware, i,ref MainCharacter);
            while (!levels[i].getFinished())
                levels[i].Show();

            currentLevel = i;
        }
        score_total = MainCharacter.Score;
        if(MainCharacter.IsDead())
            //temporal Test
            Console.WriteLine("Player dead");
    }

    public void ResetGame()
    {
        //reboot all levels
        for (int i = 0; i <= currentLevel; i++)
            levels[i].setFinished(false);
        //reboot character
        MainCharacter = new Character();
    }

    public int GetScore()
    {
        return score_total;
    }

    public void SetScore(int score)
    {
        this.score_total = score;
    }

    public Character GetCharacter()
    {
        return MainCharacter;
    }

    public bool GetExit()
    {
        return exit;
    }
}
﻿/*(c)Antonio defez
* 
* It is a list of sprite that gathers the basic information of lists
*/
public class ListSprite
{

    public virtual Sprite Get(int cont)
    {
        return null;
    }

    public  virtual int Count()
    {
        return 0;
    }

    public virtual void  Add(Sprite otherSprite)
    {
    }

    public virtual void Set(Sprite otherSprite,int index)
    {

    }

    public virtual void Draw(Hardware hardware)
    {
    }
}﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */
//third implementatio of Map class

/*The type of blocks on the map decide how many enemies are on this map, as well
as their location.
Also decides the initial situation of the player as well as the final block 
of the level
*/
using System;
using System.Xml;

class Map
{
    protected string map_img_url;
    /*We leave an empty space between the screen and the drawing of the map so that the gui*/
    public const int GUI_HEIGHT = Gui.GUI_IMAGE_HEIGHT;
    
    protected int total_height_block;
    protected int total_width_block;
    protected  Block[,] array;
    protected byte[,] matrix;
    protected int total_enemy;

    protected int total_x;      //variables for scroll
    protected int total_y;      //variables for scroll
    protected int map_x;
    protected int map_y;
    public int TOTAL_X
    {
        get { return total_x; }
        set { total_x = value; }
    }
    public int TOTAL_Y
    {
        get { return total_y; }
        set { total_y = value; }
    }
    public int MAP_X
    {
        get { return map_x; }
        set { map_x = value; }
    }
    public int MAP_Y
    {
        get { return map_y; }
        set { map_y = value; }
    }

    private bool isFinished;
    public  bool IsFinished
    {
        get
        {
            return isFinished;
        }

        set
        {
            isFinished = value;
        }
    }
    protected int width_map;
    public int WIDTH_MAP
    {
        get { return width_map; }
        set { width_map = value; }
    }
    protected int heigth_map;
    public int HEIGTH_MAP
    {
        get { return heigth_map; }
        set { heigth_map = value; }
    }

 

    //TO DO IMPLEMENT A GROUP OF BLOCK COLLISIONABLES
    protected const int collisionable = 7;
    public Map(int index)
    {
        Console.WriteLine("Map of level"+index);
        map_img_url = "maps/"+index+".tmx";
        TmxToBinary(map_img_url);

        Image[,] array = new Image[total_width_block, total_height_block];
        BuildMap(); 
        WIDTH_MAP= total_width_block * (int)Block.WIDTH_BLOCK;
        HEIGTH_MAP = total_height_block * (int)Block.HEIGHT_BLOCK;
        total_x = 0;
        total_y = 0;
        IsFinished = false;
    }

    //Converts a tmx file to a binary array that
    public void TmxToBinary(string map)
    {
        
        matrix = null;
        XmlTextReader reader = new XmlTextReader(map);
        int width = 0;
        int height = 0;
        int cont_total = 0;
        int cont_row = 0;
        int count_file = 0;
        string source = "";

        while (reader.Read())
        {
            switch (reader.NodeType)
            {
                case XmlNodeType.Element:

                    string name = reader.Name;
                    if (name == "layer")
                    {
                        /*We get the width and height of the map*/
                        width = Convert.ToInt32(reader.GetAttribute("width"));
                        height = Convert.ToInt32(reader.GetAttribute("height"));
                    }

                    else if (name == "image")
                    {
                        /*We get the image where the sprite sheet is 
                         * used on the map*/
                        source = reader.GetAttribute("source");
                    }

                    break;
                case XmlNodeType.Text:
                    //We begin to read the distribution of the
                    //different sprites that form the map set
                    string encodedString = reader.Value;
                    //We get all the map information in one-dimensional format
                    byte[] data = Convert.FromBase64String(encodedString);

                    //We pass the information to a two-dimensional matrix
                    matrix = new byte[height, width];
                    while (cont_total < data.Length)
                    {
                        if (count_file == width)
                        {
                            if (cont_row + 1 < height)
                                cont_row++;
                            count_file = 0;
                        }
                        if (cont_total < data.Length)
                            matrix[cont_row, count_file] = data[cont_total];
                        cont_total += 4;
                        count_file++;
                    }
                    total_width_block = width;
                    total_height_block = height;
                    break;
            }
        }
        //delay for console
        for (int t = 0; t < height; t++)
        {
            for (int to = 0; to < width; to++)
                Console.Write(matrix[t, to]);
            Console.WriteLine();
        }
    }

    public Block[,] GetArray()
    {
        return array;
    }

    public void BuildMap()
    {
        // 1 -> For each iteration of matrix I have to create an image
        // with new images_file
        // 2 -> the images have a width and a height of 32
        // 3 -> place them in the same way
        // name of files -> 1_1.png
        array = new Block[total_width_block, total_height_block];
        for (int co_row = 0; co_row < total_height_block; co_row++)
            for (int f_file = 0; f_file < total_width_block; f_file++)
            {
                string filename = matrix[co_row, f_file] + "_1" + ".png";
                //block of enemy
                if (matrix[co_row, f_file] == MovingEnemy.BLOCK_ID
                    || matrix[co_row, f_file] == FixedEnemy.BLOCK_ID)
                {
                    array[f_file, co_row] = new Block(filename
                                                  , false,
                                                  matrix[co_row, f_file],false);
                    array[f_file, co_row].IsEnemy = true;
                    total_enemy++;
                }
                //the only block with collison is non collisionable
                else if (matrix[co_row, f_file] == Block.COLLISION_ID)
                    array[f_file, co_row] = new Block(filename
                                                      , false,
                                                      matrix[co_row, f_file],false);
                else if (matrix[co_row, f_file] == Block.FINAL_ID)
                {
                    array[f_file, co_row] = new Block(filename
                                  , false,
                                  matrix[co_row, f_file],true);
                }
                else
                {
                    //the rest of block will be collisonable
                    array[f_file, co_row] = new Block(filename
                                                      , true,
                                                      matrix[co_row, f_file],false);
                }
                array[f_file, co_row].SetX((short)(f_file * Block.HEIGHT_BLOCK));
                array[f_file, co_row].SetY((short)((co_row * Block.WIDTH_BLOCK)
                                                    + GUI_HEIGHT));
            }
    }

    public void Draw(Hardware hardware)
    {
        Block[,] array = this.GetArray();
        foreach (Block block in array)
        {
            if (block != null)
            {
                block.MoveTo((short)(block.GetX()), (short)(block.GetY()));
                hardware.DrawImage(block);
            }
        }
    }

    public void CheckCollision(Sprite sprite )
    {
        Image imgCharacter = sprite.getImage();
        foreach (Block t in array)
        {
            if (imgCharacter.CollidesWith(t, (short)sprite.GetSpriteWidth(),
                                             (short)sprite.GetSpriteHeight(),
                                             (short)Block.WIDTH_BLOCK,
                                             (short)Block.WIDTH_BLOCK))
            {
                if (t.Collision_check)
                {
                    sprite.AfterCollisionMap();
                }
                if (t.IsFinal)
                {
                    if (sprite.GetType().ToString() == "Character")
                    {
                        IsFinished = true;
                    }
                }
                //TODO IMPLEMENTS BLOCK DESTROYED


            }
        }
    }

    public void CheckCollisonGroup(ListSprite spriteGroup)
    {

        for (int i = 0; i < spriteGroup.Count(); i++)
        {
            CheckCollision(spriteGroup.Get(i));
        }
    }

    public int GetTotalEnemy()
    {
        return total_enemy;
    }


    //To implement the scroll we will have to move the entire map set block
    //by block.This movement will be defined by the movement of the player
    public void MoveMap()
    {
        foreach (Image t in array)
        {
            if (t != null)
            {
                t.MoveTo((short)(t.GetX() + map_x), (short)(t.GetY() +map_y));
            }
        }
        map_y = 0;
        map_x = 0;
    }
    /*
     * We have to increase the position of the enemies/bullets depending on the scroll
     */

    public void ScrollEnemyShoot(EnemyGroup enemies,Character player)
    {
        for (int i = 0; i < enemies.Count(); i++)
        {
            Enemy current =(Enemy)enemies.Get(i);
            if (current.getShotList() != null)
            {
                LocatedSpritesScroll(current.getShotList());
                CheckCollisonGroup(current.getShotList());
                player.BulletCollision(current);
            }
        }
    }


    public void LocatedSpritesScroll(ListSprite spriteslist)
    {
        Sprite currentSprite = null;
        for(int i=0; i<spriteslist.Count();i++)
        {
            currentSprite = spriteslist.Get(i);
            if (currentSprite != null)
            {
               currentSprite.MoveTo((short)(currentSprite.GetX() + map_x),
                    (short)(currentSprite.GetY() + map_y));
            }
        }
           
       
    }

}
﻿/*
 Antonio Defez Ibanez
 ies san vicente

This enemy follows the character and it hurts him
 */


class MovingEnemy : Enemy
{
    public const string ENEMY_IMAGE_MOVEMENT = "imgs/movingEnemy.png";
    public const int BLOCK_ID = 27;

    public MovingEnemy() : base(ENEMY_IMAGE_MOVEMENT)
    {
        Movement_increment = 1;
    }

    /*
     * The enemy type movement will follow the player
     */
    public override void Move(Character CharacterToFollow, int level_width,
        int level_height)
    {
        Movement_increment = 1;
        int key = -3;
        if (this.DistanceToObject(CharacterToFollow, level_width, level_height))
        {
            if (CharacterToFollow.GetX() > this.GetX())
                key = Sprite.MOVE_RIGHT;

            else if (CharacterToFollow.GetX() < this.GetX())
                key = Sprite.MOVE_LEFT;

            else if (CharacterToFollow.GetY() > this.GetY())
                key = Sprite.MOVE_DOWN;
            else
                key = Sprite.MOVE_UP;

            base.Move_Sprite(key);
        }
    }
}

﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */
/*
* We will use this class as interface to speed up the creation of windows,
* and avoid using SDL-TAO directly
* changes:
* 0.1 Initial version, based on Proyect Centipede
 */

public class Screen
{
    protected Hardware hardware;

    public Screen(Hardware hardware)
    {
        this.hardware = hardware;
    }

    public virtual void Show()
    {
    }
}


﻿/*
 Antonio Defez Ibanez
 ies san vicente
This class stores all the information of the Shot */

using System;
public class Shot : Sprite
{
    public const string SHOT_IMAGE = "imgs/bullet1.png";
    public const int SHOT_IMAGE_WIDTH = 16;
    public const int SHOT_IMAGE_HEIGHT = 16;
    public const int SHOT_MOVEMENT_INCREMENT = 32;
    private bool checkCollsion;

    public bool CheckCollsion
    {
        get
        {
            return checkCollsion;
        }

        set
        {
            checkCollsion = value;
        }
    }

    public Shot(int direction, int x, int y) : base(SHOT_IMAGE, SHOT_IMAGE_WIDTH,
        SHOT_IMAGE_HEIGHT, x, y, SHOT_MOVEMENT_INCREMENT)
    {
        this.Direction = direction;
        CheckCollsion = false;
    }

    
    public void Move()
    {
        this.Move_Sprite(Direction);
    }

    /*When a bullet hits the map disappears*/
    public override  void AfterCollisionMap()
    {
        //temporal test
        CheckCollsion = true;
    }
}
﻿/*Antonio defez
 19/05/2017
 This class takes care of the set of shots of a sprite 
 (they can shoot character and enemy fixed)
 Its functionalities are draw, collision ..*/

using System.Collections.Generic;
using System;
public class ShotList :ListSprite
{
    protected List<Shot> shotList;
    protected int lastShootTime;
    protected double timeElapsed;

    public ShotList(double timeElapsed)
    {
        shotList = new List<Shot>();
        lastShootTime = 0;

        //
        this.timeElapsed = timeElapsed;
    }

    public override void Add(Sprite shot)
    {
        int currentShootTime = DateTime.Now.Second;
        if (lastShootTime >= 60)
            lastShootTime = 0;

        if (Math.Abs((double)lastShootTime-(double)currentShootTime)>timeElapsed)
        {
            shotList.Add((Shot)shot);
            lastShootTime = DateTime.Now.Second;
        }
    }

    public bool CanShoot()
    {
        int currentShootTime = DateTime.Now.Second;
        return Math.Abs((double)lastShootTime - (double)currentShootTime) > timeElapsed;
    }
    public override Sprite Get(int cont)
    {
        return shotList[cont];
    }

    public override void Set(Sprite otherSprite,int index)
    {
        shotList[index] = (Shot)otherSprite;
    }

    public override int Count()
    {
        return shotList.Count;
    }

   public void MoveShoot()
    {
        //move shoot   
        foreach (Shot s in shotList)
            s.Move();
    }

    //Draw all element of the shotlist and check status
    public  override void  Draw(Hardware hardware)
    {
        for (int i = 0; i < shotList.Count; i++)
        {
            if (shotList[i].CheckCollsion)
            {
                shotList.Remove(shotList[i]);
            }
            else
            {
                shotList[i].Draw(hardware);
            }
        }
    }
}

﻿/*
 *  Antonio defez
 *  The sprite class exists to cover all the functionalities that have 
 *  in common the elements that move.Animations ...
 */

public class Sprite
{
    public const int MOVE_UP= 3;
    public const int MOVE_LEFT = 1;
    public const int MOVE_RIGHT = 2;
    public const int MOVE_DOWN = 0;

    protected  short sprite_image_width ;
    protected short sprite_image_height ;
    protected Image image_sprite;
    protected string image_direction;
    private int movement_increment;
    protected int current_sprite_x;
    protected int current_sprite_y;
    private int direction;

    public int Direction
    {
        get
        {
            return direction;
        }

        set
        {
            direction = value;
        }
    }

    public int Movement_increment
    {
        get
        {
            return Movement_increment1;
        }

        set
        {
            Movement_increment1 = value;
        }
    }

    public int Movement_increment1
    {
        get
        {
            return movement_increment;
        }

        set
        {
            movement_increment = value;
        }
    }
    private double timeElapse;       //Break time between shots
    public  double TimeElapse
    {
        get
        {
            return timeElapse;
        }

        set
        {
            timeElapse = value;
        }
    }   
    public Sprite()
    {
        //Builder empty, I need this to easily create generic enemies
    }

    public Sprite(string image_direction,int width,int height )
    {
        sprite_image_width = (short)width;
        sprite_image_height = (short)height;
        image_sprite = new Image(image_direction, sprite_image_width,
                                                sprite_image_height);
    }

    public Sprite(string image_direction, int width, int height, int x, int y,
        int movement_increment)
        :this(image_direction, width, height)
    {
        this.image_sprite.SetX((short)x);
        this.image_sprite.SetY((short)y);
        this.Movement_increment = movement_increment;
    }

 


    public Image getImage()
    {
        return image_sprite; 
    }

    public virtual void Draw(Hardware hardware)
    {
        hardware.DrawImage(image_sprite, 
            (short)(current_sprite_x * sprite_image_width),
            (short)(current_sprite_y * sprite_image_height), sprite_image_width,
            sprite_image_height);
    }

    /*
    It receives that movement it will make next the sprite, moves the image
    according to the previous one and it connects it to its corresponding 
    animation*/
    public void  Move_Sprite(int cdirection)
    {
        Direction = cdirection;
        if (cdirection == Sprite.MOVE_LEFT)
        {
            image_sprite.MoveTo((short)(image_sprite.GetX() - Movement_increment),
            image_sprite.GetY());
            UpdateAnim(MOVE_LEFT);
        }
        else if (cdirection == Sprite.MOVE_RIGHT)
        {
            image_sprite.MoveTo((short)(image_sprite.GetX() + Movement_increment),
            image_sprite.GetY());
            UpdateAnim(MOVE_RIGHT);
        }
        else if (cdirection == Sprite.MOVE_UP)
        {
            image_sprite.MoveTo((short)(image_sprite.GetX()),
                                (short)(image_sprite.GetY() - Movement_increment));
            UpdateAnim(MOVE_UP);

        }
        else if (cdirection == Sprite.MOVE_DOWN)
        {
            image_sprite.MoveTo((short)(image_sprite.GetX()),
                                (short)(image_sprite.GetY() + Movement_increment));
            UpdateAnim(MOVE_DOWN);
        }
    }
    /*
     * Update variables to perform animation
     */
    public void UpdateAnim(int move)
    {
        current_sprite_y = move;
        if (current_sprite_x >= 2)
        {
            current_sprite_x = 0;
        }
        else
        {
            current_sprite_x++;
        }
    }

    public short GetX()
    {
        return image_sprite.GetX();
    }

    public short GetY()
    {
        return image_sprite.GetY();
    }


    public bool CollidesWith(Sprite OtherSprite, short w1, short h1, short w2, short h2)
    {
        Image img = OtherSprite.getImage();
        return image_sprite.CollidesWith(img, w1, h1, w2, h2);
    }

    public void MoveTo(short x, short y)
    {
        this.image_sprite.MoveTo(x, y);
    }

    public int GetSpriteWidth()
    {
        return image_sprite.GetImageWidth();
    }

    public int GetSpriteHeight()
    {
        return image_sprite.GetImageHeight();
    }

   


    /*
     * 
    After an impact, there will be a change in the state of the sprite
     */
    public virtual void  AfterCollisionMap()
    {
        if (this.direction == Sprite.MOVE_LEFT)
        {
            this.MoveTo((short)(this.GetX() + movement_increment),
                this.GetY());
        }
        else if (this.direction == Sprite.MOVE_RIGHT)
        {
            this.MoveTo((short)(this.GetX() - movement_increment),
                this.GetY());
        }
        else if (this.direction == Sprite.MOVE_UP)
        {
            this.MoveTo(this.GetX(),
                (short)(this.GetY() + movement_increment));
        }
        else if (this.direction == Sprite.MOVE_DOWN)
        {
            this.MoveTo(this.GetX(),
                (short)(this.GetY() - movement_increment));
        }
    }

    public virtual void AfterCollisionBullet()
    {

    }

}

﻿/*
 Antonio Defez Ibanez
 ies san vicente
 
 Zombie Survival game starter class
 */

class SurvivalZombie 
{
   static void Main(string[] args)
   {
       GameController controller = new GameController();
       controller.Start();

   }
}

﻿/*
 Antonio Defez Ibanez
 ies san vicente
 */

class WelcomeScreen : Screen
{
    const string WELCOME_IMAGE = "imgs/zombie_index.png";
    const int WELCOME_IMAGE_WIDTH = 1000;
    const int WELCOME_IMAGE_HEIGHT = 250;
    protected Audio backgroundSong;
    protected Image imgTitle;
    bool exit;

    public WelcomeScreen(Hardware hardware) : base(hardware)
    {
        exit = false;
        backgroundSong = new Audio(44100, 2, 4096);


         imgTitle = new Image(WELCOME_IMAGE, WELCOME_IMAGE_WIDTH,
            WELCOME_IMAGE_HEIGHT);

       
        backgroundSong.AddMusic("music/intro.mid");
    }

    public override void Show()
    {
        bool escPressed = false, spacePressed = false, qPressed = false;
        CreditsScreen creditsScreen = new CreditsScreen(hardware);
        Font myFont = new Font(Hardware.GAME_FONT, 20);
        imgTitle.MoveTo(Hardware.DEFAULT_WIDTH / 2 - WELCOME_IMAGE_WIDTH / 2,
            Hardware.DEFAULT_HEIGHT / 3 - WELCOME_IMAGE_HEIGHT / 2);
        hardware.DrawImage(imgTitle);
        hardware.WriteText("Press SpaceBar to go on or Escape to exit",
            Hardware.DEFAULT_WIDTH / 2 - 250, Hardware.DEFAULT_HEIGHT * 2 / 3
            , 255, 255, 0, myFont);
        hardware.WriteText("Press 1 to see the high scores", 
            Hardware.DEFAULT_WIDTH / 2 - 175, Hardware.DEFAULT_HEIGHT / 3 + 225,
            255, 255, 0, myFont);
        hardware.UpdateScreen();

      
        backgroundSong.PlayMusic(0, -1);
        do
        {
          
            int keyPressed = hardware.KeyPressed();
            if (keyPressed == Hardware.KEY_ESC)
            {
                escPressed = true;
                exit = true;
            }
            else if (keyPressed == Hardware.KEY_SPACE)
            {
                spacePressed = true;
                exit = false;
            }
            else if (keyPressed == Hardware.KEY_1)
            {
                qPressed = true;
                creditsScreen.Show();
                exit = false;
            }
            //Pause
            System.Threading.Thread.Sleep(10);
        }
        while (!escPressed && !spacePressed && !qPressed);
       
    }

    public bool GetExit()
    {
        return exit;
    }
}

